Proyecto final de la asignatura Diseño Asistido por Computador. Curso 2011 - 2012.

Resumen:

El proyecto consiste en una aplicación gráfica interactiva en 3D que permite el diseño de viviendas similar al que podemos encontrar en la serie de videojuegos "The Sims". Desarrollado en el lenguaje de programación C usando la librería gráfica OpenGL.

Descripción:

La aplicación, a la que he llamado **House Builder**, nos permite realizar el diseńo en tres dimensiones de una vivienda de una planta de forma interactiva.
Para ello nos permite colocar una serie de elementos:

* Muro exterior
* Muro interior
* Puerta de muro exterior
* Puerta de muro interior
* Ventana simple
* Ventana doble
* Silla
* Sillón
* Sofá
* Mesa pequeńa
* Mesa grande
* Estantería
* Cama
* Mesita de noche
* Armario
* Bańera
* WC
* Lavabo
* Frigorífico
* Cajonera de cocina
* Armario de cocina
* Hornilla

Para facilitarnos el diseño, la aplicación cuenta con tres herramientas que se pueden activar y desactivar según nuestras necesidades: la primera nos muestra la disposición de los ejes de coordenadas, la segunda nos proporciona una cuadrícula (en la que cada hueco es 1 m2), y la tercera nos permite mostrar sólo la parte baja de los muros para facilitarnos la visualización de los elementos que tengamos colocados tras ellos.

Una vez realizado nuestro diseño, la aplicación nos permitirá guardarlo para poder cargarlo cuando estimemos oportuno. Los archivos de guardado generados tendrán extensión hb.

Para las operaciones de guardado, carga, y apertura de un diseño vacío se pedirá confirmación a través del terminal que lanzó la aplicación. La ayuda de los controles de la cámara se mostrará también en dicha terminal.

El ejecutable del proyecto es el archivo "house_builder" y está compilado para Linux.

El código para gestionar la ventana de visualización y el Makefile nos fue dado por los profesores de la asignatura. El código de la aplicación y los modelos fue escrito por Juan Antonio Fajardo Serrano.

Capturas de pantalla de la aplicación en funcionamiento:

![casa1.jpg](https://bitbucket.org/repo/zqGbXr/images/3550096201-casa1.jpg)

![casa2.jpg](https://bitbucket.org/repo/zqGbXr/images/2485237268-casa2.jpg)

![casa3.jpg](https://bitbucket.org/repo/zqGbXr/images/2426616893-casa3.jpg)

![casa4.jpg](https://bitbucket.org/repo/zqGbXr/images/291333810-casa4.jpg)

![casa5.jpg](https://bitbucket.org/repo/zqGbXr/images/4034638769-casa5.jpg)