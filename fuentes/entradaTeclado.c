/*	Grua 

	C.A.D. 						Curso 2008-2009
 	
	Codigo base para la realización de las practicas de CAD

	modulo entradaTeclado.c
	Gestion de eventos de teclado
=======================================================
	J.C. Torres 
	Dpto. Lenguajes y Sistemas Informticos
	(Univ. de Granada, SPAIN)

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details 
 http://www.gnu.org/copyleft/gpl.html

=======================================================
Queda prohibido cobrar canon por la copia de este software

*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>                   // Libreria de utilidades de OpenGL
#include "grua.h"

/** 

Imprime en la consola las instrucciones del programa

**/
void printHelp(){
  	
	printf("\n\n        C.A.D. 		Curso 2011-2012");
  	printf("\n\n Dpto. Lenguajes y Sistemas Informaticos");
  	printf("\n E.T.S.I. Informatica		Univ. de Granada ");
  	printf("\n");
  	printf("\n Opciones: \n\n");
  	printf("\n*** Control de cámara ***\n");
	printf("\nVista 2D");
	printf("\n   En este modo:");
	printf("\n   x Desplazar la cámara en el eje X en dirección negativa");
	printf("\n   X Desplazar la cámara en el eje X en dirección positiva");
	printf("\n   z Desplazar la cámara en el eje Z en dirección negativa");
	printf("\n   Z Desplazar la cámara en el eje Z en dirección positiva");
	printf("\n   + Acercar la cámara al suelo");
	printf("\n   - Alejar la cámara del suelo");
	printf("\n\nVista 3D");
	printf("\n   En este modo:");
	printf("\n   x Desplazar la cámara en el eje X en dirección negativa");
	printf("\n   X Desplazar la cámara en el eje X en dirección positiva");
	printf("\n   z Desplazar la cámara en el eje Z en dirección negativa");
	printf("\n   Z Desplazar la cámara en el eje Z en dirección positiva");
	printf("\n   c Aumentar altura de cámara en perspectiva");
	printf("\n   C Disminuir altura de cámara en perspectiva");
	printf("\n   PageUp Avanzar cámara en la dirección en la que se mira");
	printf("\n   PageDown Alejar cámara en la dirección en la que se mira");
	printf("\n   Flecha arriba Inclinar cámara hacia abajo");
	printf("\n   Flecha abajo Inclinar cámara hacia arriba");
	printf("\n   Flecha izquierda Girar cámara hacia la izquierda");
	printf("\n   Flecha derecha Girar cámara hacia la derecha");
	/*printf("\n\n*** Controles del operario ***\n");
	printf("\n   O Girar la cabeza a la izquierda");
	printf("\n   o Girar la cabeza a la derecha");
	printf("\n   K Bajar el brazo izquierdo");
	printf("\n   k Subir el brazo izquierdo");
	printf("\n   L Bajar el brazo derecho");
	printf("\n   l Subir el brazo derecho");
	printf("\n   P Poner la boca triste :(");
	printf("\n   p Poner la boca alegre :)");
	printf("\n   I Incorporarse");
	printf("\n   i Sentarse");
	printf("\n\n*** Controles de posición de la grúa ***\n");
	printf("\n   Q Girar brazo en sentido antihorario");
	printf("\n   q Girar brazo en sentido horario");
	printf("\n   W Desplazar carro hacia la punta de la flecha");
	printf("\n   w Desplazar carro hacia la cabina de control");
	printf("\n   E Bajar gancho");
	printf("\n   e Subir gancho");
	printf("\n\n*** Controles de velocidad de la grúa ***\n");
	printf("\n   A Aumentar velocidad de giro del brazo en sentido antihorario");
	printf("\n   a Aumentar velocidad de giro del brazo en sentido horario");
	printf("\n   S Aumentar velocidad de desplazamiento del carro hacia la punta de la flecha");
	printf("\n   s Aumentar velocidad de desplazamiento del carro hacia la cabina de control");
	printf("\n   D Aumentar velocidad de bajada del gancho");
	printf("\n   d Aumentar velocidad de subida del gancho");
	printf("\n   Nota: Para cada control hay 10 niveles de velocidad");
	printf("\n   F,f Poner a 0 todas las velocidades");
	printf("\n\n   Primera palanca de la interfaz");
	printf("\n      Hacia arriba: Aumentar velocidad de giro del brazo en sentido horario");
	printf("\n      Hacia abajo: Aumentar velocidad de giro del brazo en sentido antihorario");
	printf("\n   Segunda palanca de la interfaz");
	printf("\n      Hacia arriba: Aumentar velocidad de desplazamiento del carro hacia la punta de la flecha");
	printf("\n      Hacia abajo: Aumentar velocidad de desplazamiento del carro hacia la cabina de control");
	printf("\n   Tercera palanca de la interfaz");
	printf("\n      Hacia arriba: Aumentar velocidad de subida del gancho");
	printf("\n      Hacia abajo: Aumentar velocidad de bajada del gancho");*/
	printf("\n\n*** Otros ***\n");
	//printf("\n   H,h Imprimir ayuda");
    printf("\n   Boton derecho del raton activa el menu");
	//printf("\n   Escape: Salir");
  	printf("\n\n\n");
}




/* @teclado ---------------------------------------------------------------- */

/** 		void letra (unsigned char k, int x, int y)

Este procedimiento es llamado por el sistema cuando se pulsa una tecla normal
El codigo k es el ascii de la letra

Para anyadir nuevas ordenes de teclado coloca el correspondiente case.

Parametros de entrada:

k: codigo del caracter pulsado

x:

y:

**/

void letra (unsigned char k, int x, int y)
{
	switch (k) {
		case 'h':
		case 'H':
			seleccionMenu(AYUDA);
			break;
		case '3':
			seleccionMenu(PASEAR);
			break;
		case '2':
			seleccionMenu(DESDEARRIBA);
			break;
		case 'c':	// desplaza la camara en Y
			if(VISTA_PERSPECTIVA) y_camara += VELDESPCAMARA;
			break;
		case 'C':
			if(VISTA_PERSPECTIVA) y_camara -= VELDESPCAMARA;
			break;
		case 'x':	// desplaza la camara en X
			if(VISTA_PERSPECTIVA)   x_camara -= VELDESPCAMARA;   
			else origenXVentanaMundoParalelo -= VELDESPCAMARA;
			break;
		case 'X':
			if(VISTA_PERSPECTIVA)   x_camara += VELDESPCAMARA;   
			else origenXVentanaMundoParalelo += VELDESPCAMARA;
			break;
		case 'z':	// desplaza la camara en Z
			if(VISTA_PERSPECTIVA)   z_camara -= VELDESPCAMARA;   
			else origenYVentanaMundoParalelo -= VELDESPCAMARA;
			break;
		case 'Z':
			if(VISTA_PERSPECTIVA)   z_camara += VELDESPCAMARA;   
			else origenYVentanaMundoParalelo += VELDESPCAMARA;
			break;
		case '+':             // + Camara mas cerca
			if(ventanaMundoParalela>10) ventanaMundoParalela = ventanaMundoParalela*0.75;
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			fijaProyeccion();
			break;
		case '-':             // - Camara mas lejos
			ventanaMundoParalela =ventanaMundoParalela* 1.25;
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			fijaProyeccion();
			break;
		// Enter
		case 13:
			if(estado == PoniendoMuebles){
				confirmaMueble();
			}
			else if(estado == PoniendoMuros){
				confirmaMuro();
			}
			else if(estado == PoniendoElementos){
				confirmaElemento();
			}
			// Nota: No se puede agrupar, ya que el enter se usa para escribir en consola
			break;
		// Suprimir
		case 127:
			seleccionMenu(ELIMINAR);
			break;
		case 'l':
		case 'L':
			seleccionMenu(ALTPUNTO);
			break;
		case 'e':
		case 'E':
			seleccionMenu(ACDEEJES);
			break;
		case 't':
		case 'T':
			seleccionMenu(ALTCUAD);
			break;
		case 'm':
		case 'M':
			seleccionMenu(ACDEMUROS);
			break;
		case 'r':
		case 'R':
			seleccionMenu(ALTROT);
			break;
		case 'g':
		case 'G':
			opcion_guardar = 0;
			guardado();
			break;
		case 'q':
		case 'Q':
			seleccionMenu(SALIR);
			break;
		case 'n':
		case 'N':
			nueva_escena();
			break;
		case 'a':
		case 'A':
			cargado();
			break;
		// Escape  Volver
		case 27:
			seleccionMenu(VOLVER);
			break;
		default:
			//printf("\nTecla pulsada: %i\n",k);
			return;
	}
	glutPostRedisplay();  	// Algunas de las opciones cambian paramentros
}                       // de la camara. Es necesario actualizar la imagen

/**		void especial(int k, int x, int y)
Este procedimiento es llamado por el sistema cuando se pulsa una tecla
especial. El codigo k esta definido en glut mediante constantes

Parametros de entrada:

k: codigo del caracter pulsado (definido en glut mediante constantes).

x:

y:

**/
void especial(int k, int x, int y)

{
  switch (k) {
  case GLUT_KEY_UP:
    if(VISTA_PERSPECTIVA)     view_rotx += 5.0;   // Cursor arriba + rotacion x
    break;
  case GLUT_KEY_DOWN:
    if(VISTA_PERSPECTIVA)    view_rotx -= 5.0;
    break;
  case GLUT_KEY_RIGHT:
    if(VISTA_PERSPECTIVA) {
	view_roty += 5.0;
    	if(view_roty>360) view_roty-=360;
	}
    break;
  case GLUT_KEY_LEFT:
    if(VISTA_PERSPECTIVA) {
    	view_roty -= 5.0;
    	if(view_roty<0) view_roty+=360;
	}
    break;
  case GLUT_KEY_PAGE_DOWN:  	// Avanza en la direccion en la que se mira
     if(VISTA_PERSPECTIVA) {
     	x_camara+=-sin(3.14159*view_roty/180.0) * VELDESPCAMARA;
     	z_camara+= cos(3.14159*view_roty/180.0) * VELDESPCAMARA;
	}
     break;
  case GLUT_KEY_PAGE_UP:
    if(VISTA_PERSPECTIVA) {
     	x_camara+= sin(3.14159*view_roty/180.0) * VELDESPCAMARA;
     	z_camara+=-cos(3.14159*view_roty/180.0) * VELDESPCAMARA;
	}
     break;
  default:
    return;
  }
   glutPostRedisplay();  // Actualiza la imagen (ver proc. letra)
}
 
