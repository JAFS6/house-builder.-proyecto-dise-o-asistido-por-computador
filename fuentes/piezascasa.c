#include "grua.h"

void confirmaMuro()
{
	if(punto_colocando == 0){
		punto_colocando = 1;
		estado_anadir_muro = 2;
		gluiOutput("Situe el final del muro");
	}
	else{
		estado_anadir_muro = 0;
		punto_colocando = 0;
		if(anadir_muro(&nuevo_muro) == -1){
			gluiOutput("Limite alcanzado");
		}
		nuevo_muro.puntos[0].x = nuevo_muro.puntos[1].x;
		nuevo_muro.puntos[0].y = nuevo_muro.puntos[1].y;
		gluiOutput("Situe el inicio del muro");
	}
}

void inicializacion_muro()
{
	estado = PoniendoMuros;
	if(estado_menu == 0){ // Si no est� puesto el men� contextual lo ponemos
		CreaMenu();
	}
	nuevo_muro.color = colorActivo;
	nuevo_muro.puntos[0].x = 0;
	nuevo_muro.puntos[0].y = 0;
	nuevo_muro.puntos[1].x = 0;
	nuevo_muro.puntos[1].y = 0;
	gluiOutput("Situe el inicio del muro");
	estado_anadir_muro = 0;
	punto_colocando = 0;
}

/**
	Inicializa el conjunto de muros
**/
void inicializa_conj_muros()
{
	int i;
	for(i=0; i<MAXMUROS; ++i){
		conjunto_muros[i].libre = 1;
	}
	num_muros_conjunto = 0;
}

/**
	A�ade un muro al conjunto de muros
	nuevo: El muro a a�adir
	Devuelve 0 en caso de �xito, y -1 en caso de fallo
**/
int anadir_muro(MURO * nuevo)
{
	// Buscamos posici�n donde a�adirlo
	int pos = num_muros_conjunto; // Posici�n a partir de la que buscaremos
	int i,encontrado = 0;
	for(i=0; encontrado == 0 && (i<MAXMUROS); ++i){ // Como mucho daremos una vuelta al vector
		if(conjunto_muros[pos].libre == 1){
			encontrado = 1;
		}
		else{
			pos = (pos+1)%MAXMUROS;
		}
	}
	
	if(encontrado == 1){
		// A�adimos el muro copiando su configuraci�n
		conjunto_muros[pos].muro.tipo = nuevo->tipo;
		conjunto_muros[pos].muro.grosor = nuevo->grosor;
		conjunto_muros[pos].muro.color = nuevo->color;
		conjunto_muros[pos].muro.puntos[0] = nuevo->puntos[0];
		conjunto_muros[pos].muro.puntos[1] = nuevo->puntos[1];
		
		// Marcamos la posici�n como ocupada
		conjunto_muros[pos].libre = 0;
		// Aumentamos el contador de muros en el conjunto
		++num_muros_conjunto;
		return 0;
	}
	return -1;
}

/**
	Elimina un muro del conjunto de muros
	i: El �ndice del muro dentro del conjunto de muros
**/
void eliminar_muro(int i)
{
	conjunto_muros[i].libre = 1;
	--num_muros_conjunto;
}

/**
	Pinta el muro pasado como argumento
	m: El muro a pintar
**/
void pintaMuro(MURO * m)
{	
	glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[m->color]);
	
	pintaSeccion(m->puntos[0],m->puntos[1],m->grosor,alturaMuro());
}

/**
	Devuelve la altura a la que se pintar�n los muros
**/
float alturaMuro()
{
	if(muro_oculto == 0){
		return ALTURA_MURO;
	}
	else{
		return ALTURA_MURO_OC;
	}
}

/**
	Pinta una secci�n de muro entre los puntos p1 y p2
	p1: Extremo de la secci�n
	p2: El otro extremo de la secci�n
	ancho: ancho de la secci�n
	alto: alto de la secci�n
**/
void pintaSeccion(PUNTO2D p1,PUNTO2D p2,float ancho,float alto)
{
	glPushMatrix();
		// Llevamos la secci�n hasta el punto p1
		glTranslatef(p1.x,0,p1.y); // Es y porque el punto2D solo tiene X,Y (Revisado, no tocar)
		// Rotamos para alinear con el vector p1->p2
		glRotatef(angulo_orientacion(p1.x,p1.y,p2.x,p2.y),0,1,0);
		float largo = distanciaEuclidea2D(p1.x,p1.y,p2.x,p2.y);
		// Colocamos la secci�n en X>=0
		glTranslatef(largo/2,0,0);
		/* Sumamos al largo la mitad del grosor del muro por cada lado
		largo += ancho; // 2*(ancho/2) Se va el 2 con el 2 */
		// La secci�n se pinta como una caja de tama�o X:largo y:alto z:ancho
		caja(largo,alto,ancho);
	glPopMatrix();
}

void inicializa_conj_elementos()
{
	int i;
	for(i=0; i<MAXMELEMENTOS; ++i){
		conjunto_elementos[i].libre = 1;
	}
	num_elementos_conjunto = 0;
	nuevo_elemento.tipo = -1;
}

void pintaElemento(ELEMENTO * e)
{
	glPushMatrix();
		glTranslatef(e->x,e->y,e->z);
		glRotatef(e->rot,0,1,0);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[e->color]);
		switch(e->tipo){
			case 0:
				creaVentana1();
				break;
			case 1:
				creaVentana2();
				break;
			case 2:
				creaPuerta1();
				break;
			case 3:
				creaPuerta2();
				break;
		}
	glPopMatrix();
}

int anadir_elemento(ELEMENTO * nuevo)
{
	// Buscamos posici�n donde a�adirlo
	int pos = num_elementos_conjunto; // Posici�n a partir de la que buscaremos
	int i,encontrado = 0;
	for(i=0; encontrado == 0 && (i<MAXMELEMENTOS); ++i){ // Como mucho daremos una vuelta al vector
		if(conjunto_elementos[pos].libre == 1){
			encontrado = 1;
		}
		else{
			pos = (pos+1)%MAXMELEMENTOS;
		}
	}
	
	if(encontrado == 1){
		// A�adimos el elemento copiando su configuraci�n
		conjunto_elementos[pos].elemento.tipo = nuevo->tipo;
		conjunto_elementos[pos].elemento.x = nuevo->x;
		conjunto_elementos[pos].elemento.y = nuevo->y;
		conjunto_elementos[pos].elemento.z = nuevo->z;
		conjunto_elementos[pos].elemento.color = nuevo->color;
		conjunto_elementos[pos].elemento.rot = nuevo->rot;
		conjunto_elementos[pos].elemento.cara = nuevo->cara;
		
		// Marcamos la posici�n como ocupada
		conjunto_elementos[pos].libre = 0;
		// Aumentamos el contador de elementos en el conjunto
		++num_elementos_conjunto;
		return 0;
	}
	return -1;
}

void confirmaElemento()
{
	if(pintar_muro == 1){ // Si est� a 0 es que no se ha colocado
		if(anadir_elemento(&nuevo_elemento) == -1){
			gluiOutput("Limite alcanzado");
		}
		else{
			gluiOutput("Confirmado");
		}
		pintar_muro = 0;
	}
}

void eliminar_elemento(int i)
{
	conjunto_elementos[i].libre = 1;
	--num_elementos_conjunto;
}

void inicializacion_elemento()
{
	estado = PoniendoElementos;
	if(estado_menu == 0){ // Si no est� puesto el men� contextual lo ponemos
		CreaMenu();
	}
	nuevo_elemento.x = 0;
	nuevo_elemento.y = 0;
	nuevo_elemento.z = 0;
	nuevo_elemento.color = colorActivo;
	nuevo_elemento.rot = 0;
	nuevo_elemento.cara = 0;
	gluiOutput("Situe el elemento");
	pintar_muro = 0;
}

void confirmaMueble()
{
	if(estado_anadir_mueble == 1){ // Si est� a 0 es que no se ha colocado
		estado_rotacion = 0;
		if(anadir_mueble(&nuevo_mueble) == -1){
			gluiOutput("Limite alcanzado");
		}
		else{
			gluiOutput("Confirmado");
		}
		estado_anadir_mueble = 0;
	}
}

void inicializacion_mueble()
{
	estado = PoniendoMuebles;
	if(estado_menu == 0){ // Si no est� puesto el men� contextual lo ponemos
		CreaMenu();
	}
	nuevo_mueble.x = 0;
	nuevo_mueble.y = 0;
	nuevo_mueble.z = 0;
	nuevo_mueble.color = colorActivo;
	nuevo_mueble.rot = 0;
	estado_rotacion = 0;
	gluiOutput("Situe el mueble");
	estado_anadir_mueble = 0;
}

/**
	Pinta el mueble pasado como argumento
	m: El mueble a pintar
**/
void pintaMueble(MUEBLE * m)
{
	glPushMatrix();
		glTranslatef(m->x,m->y,m->z);
		glRotatef(m->rot,0,1,0);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[m->color]);
		switch(m->tipo){
			case 0:
				creaSillon();
				break;
			case 1:
				creaSofa();
				break;
			case 2:
				creaFrigorifico();
				break;
			case 3:
				creaMesa1x1();
				break;
			case 4:
				creaSilla();
				break;
			case 5:
				creaMesaXL();
				break;
			case 6:
				creaEstanteriaXL();
				break;
			case 7:
				creaCama();
				break;
			case 8:
				creaBanera();
				break;
			case 9:
				creaWC();
				break;
			case 10:
				creaLavabo();
				break;
			case 11:
				creaMesitaNoche(m->color);
				break;
			case 12:
				creaArmario(m->color);
				break;
			case 13:
				creaCajonera(m->color);
				break;
			case 14:
				creaEncimera(m->color);
				break;
			case 15:
				creaHornilla(m->color);
				break;
		}
	glPopMatrix();
}

/**
	Inicializa el conjunto de muebles
**/
void inicializa_conj_muebles()
{
	int i;
	for(i=0; i<MAXMUEBLES; ++i){
		conjunto_muebles[i].libre = 1;
	}
	num_muebles_conjunto = 0;
	nuevo_mueble.tipo = -1;
}

/**
	A�ade un mueble al conjunto de muebles
	nuevo: El mueble a a�adir
	Devuelve 0 en caso de �xito, y -1 en caso de fallo
**/
int anadir_mueble(MUEBLE * nuevo)
{
	// Buscamos posici�n donde a�adirlo
	int pos = num_muebles_conjunto; // Posici�n a partir de la que buscaremos
	int i,encontrado = 0;
	for(i=0; encontrado == 0 && (i<MAXMUEBLES); ++i){ // Como mucho daremos una vuelta al vector
		if(conjunto_muebles[pos].libre == 1){
			encontrado = 1;
		}
		else{
			pos = (pos+1)%MAXMUEBLES;
		}
	}
	
	if(encontrado == 1){
		// A�adimos el mueble copiando su configuraci�n
		conjunto_muebles[pos].mueble.tipo = nuevo->tipo;
		conjunto_muebles[pos].mueble.x = nuevo->x;
		conjunto_muebles[pos].mueble.y = nuevo->y;
		conjunto_muebles[pos].mueble.z = nuevo->z;
		conjunto_muebles[pos].mueble.color = nuevo->color;
		conjunto_muebles[pos].mueble.rot = nuevo->rot;
		
		// Marcamos la posici�n como ocupada
		conjunto_muebles[pos].libre = 0;
		// Aumentamos el contador de muebles en el conjunto
		++num_muebles_conjunto;
		return 0;
	}
	return -1;
}

/**
	Elimina un mueble del conjunto de muebles
	i: El �ndice del mueble dentro del conjunto de muebles
**/
void eliminar_mueble(int i)
{
	conjunto_muebles[i].libre = 1;
	--num_muebles_conjunto;
}

void creaSillon()
{
	glPushMatrix();
		glRotatef(90,0,1,0);
		// Cuerpo del sill�n
		caja(1,0.5,1);
		
		//Respaldo
		glPushMatrix();
			glTranslatef(0,0.5,-0.25);
			caja(1,0.5,0.5);
		glPopMatrix();
		
		//Brazo izquierdo
		glPushMatrix();
			glTranslatef(0.525,0.25,0);
			caja(0.05,0.5,0.8);
		glPopMatrix();
		
		//Brazo derecho
		glPushMatrix();
			glTranslatef(-0.525,0.25,0);
			caja(0.05,0.5,0.8);
		glPopMatrix();
	glPopMatrix();
}

void creaSofa()
{
	glPushMatrix();
		glRotatef(90,0,1,0);
		// Cuerpo del sof�
		caja(2,0.5,1);
		
		//Respaldo
		glPushMatrix();
			glTranslatef(0,0.5,-0.25);
			caja(2,0.5,0.5);
		glPopMatrix();
		
		//Brazo izquierdo
		glPushMatrix();
			glTranslatef(1.025,0.25,0);
			caja(0.05,0.5,0.8);
		glPopMatrix();
		
		//Brazo derecho
		glPushMatrix();
			glTranslatef(-1.025,0.25,0);
			caja(0.05,0.5,0.8);
		glPopMatrix();
	glPopMatrix();
}

void creaFrigorifico()
{
	glPushMatrix();
		glRotatef(90,0,1,0);
		glTranslatef(0,0,-0.05); // Para centrar el objeto ya que est� descompensado hacia delante
		// Cuerpo del frigor�fico
		caja(1,2,1);
		
		// Puerta inferior
		glPushMatrix();
			glTranslatef(0,0.05,0.525);
			caja(0.9,1.233,0.05);
		glPopMatrix();
		// Puerta superior
		glPushMatrix();
			glTranslatef(0,1.333,0.525);
			caja(0.9,0.617,0.05);
		glPopMatrix();
		
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[gris]);
		// Tirador superior
		glPushMatrix();
			glTranslatef(-0.325,1.383,0.55);
			caja(0.15,0.05,0.05);
		glPopMatrix();
		
		// Tirador inferior
		glPushMatrix();
			glTranslatef(-0.325,1.183,0.55);
			caja(0.15,0.05,0.05);
		glPopMatrix();
	glPopMatrix();
}

void creaMesa1x1()
{
	glPushMatrix();
		// Tablero
		glPushMatrix();
			glTranslatef(0,0.4,0);
			caja(1,0.05,1);
		glPopMatrix();
		
		// Patas
		glPushMatrix();
			glTranslatef(0.25,0,0.25);
			caja(0.05,0.4,0.05);
		glPopMatrix();
		
		glPushMatrix();
			glTranslatef(-0.25,0,0.25);
			caja(0.05,0.4,0.05);
		glPopMatrix();
		
		glPushMatrix();
			glTranslatef(0.25,0,-0.25);
			caja(0.05,0.4,0.05);
		glPopMatrix();
		
		glPushMatrix();
			glTranslatef(-0.25,0,-0.25);
			caja(0.05,0.4,0.05);
		glPopMatrix();
		
	glPopMatrix();
}

void creaSilla()
{
	glPushMatrix();
		glRotatef(90,0,1,0);
		// Asiento
		glPushMatrix();
			glTranslatef(0,0.48,0);
			caja(0.4,0.04,0.4);
		glPopMatrix();
		// Respaldo
		glPushMatrix();
			glTranslatef(0,0.52,-0.19);
			caja(0.4,0.48,0.02);
		glPopMatrix();
		// Patas
		glPushMatrix();
			glTranslatef(-0.14,0,0.14);
			caja(0.04,0.48,0.04);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0.14,0,0.14);
			caja(0.04,0.48,0.04);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(-0.14,0,-0.14);
			caja(0.04,0.48,0.04);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0.14,0,-0.14);
			caja(0.04,0.48,0.04);
		glPopMatrix();
	glPopMatrix();
}

void creaMesaXL()
{
	glPushMatrix();
		// Tablero
		glPushMatrix();
			glTranslatef(0,0.75,0);
			caja(1.2,0.1,0.9);
		glPopMatrix();
		// Patas
		glPushMatrix();
			glTranslatef(-0.51,0,0.36);
			caja(0.06,0.75,0.06);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0.51,0,0.36);
			caja(0.06,0.75,0.06);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(-0.51,0,-0.36);
			caja(0.06,0.75,0.06);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0.51,0,-0.36);
			caja(0.06,0.75,0.06);
		glPopMatrix();
	glPopMatrix();
}

void creaEstanteriaXL()
{
	glPushMatrix();
		glRotatef(90,0,1,0);
		// Base
		caja(1.2,0.11,0.3);
		glTranslatef(0,0.11,0);
		// Laterales
		glPushMatrix();
			glTranslatef(-0.585,0,0);
			caja(0.03,2.09,0.3);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0.585,0,0);
			caja(0.03,2.09,0.3);
		glPopMatrix();
		// Parte de atras
		glPushMatrix();
			glTranslatef(0,0,-0.14);
			caja(1.14,2.09,0.02);
		glPopMatrix();
		// Baldas
		glTranslatef(0,0.46,0);
		caja(1.14,0.03,0.3);
		glTranslatef(0,0.49,0);
		caja(1.14,0.03,0.3);
		glTranslatef(0,0.49,0);
		caja(1.14,0.03,0.3);
		glTranslatef(0,0.49,0);
		caja(1.14,0.03,0.3);
	glPopMatrix();
}

void creaCama()
{
	float altura_patas = 0.2;
	float altura_cuerpo = 0.4;
	float altura_total = 0.6;
	glPushMatrix();
		// Cuerpo principal
		glPushMatrix();
			glTranslatef(0.25,altura_patas,0);
			caja(1.6,altura_cuerpo,1.1);
		glPopMatrix();
		// Almohada
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[blanco]);
		glPushMatrix();
			glTranslatef(-0.9,altura_total,0);
			caja(0.25,0.07,1.05);
		glPopMatrix();
		// Embozo
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[naranja]);
		glPushMatrix();
			glTranslatef(-0.65,altura_patas,0);
			caja(0.2,altura_cuerpo,1.1);
		glPopMatrix();
		// Zona almohada
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[blanco]);
		glPushMatrix();
			glTranslatef(-0.9,altura_patas,0);
			caja(0.3,altura_cuerpo,1.1);
		glPopMatrix();
		// Patas
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[marron]);
		glPushMatrix();
			glTranslatef(-0.975,0,0.475);
			caja(0.05,altura_patas,0.05);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0.975,0,0.475);
			caja(0.05,altura_patas,0.05);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(-0.975,0,-0.475);
			caja(0.05,altura_patas,0.05);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0.975,0,-0.475);
			caja(0.05,altura_patas,0.05);
		glPopMatrix();
	glPopMatrix();
}

void creaBanera()
{
	glPushMatrix();
		caja(1.38,0.1,0.58); // Base
		// Laterales largos
		glPushMatrix();
			glTranslatef(0,0,0.32);
			caja(1.5,0.6,0.06);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0,0,-0.32);
			caja(1.5,0.6,0.06);
		glPopMatrix();
		// Laterales cortos
		glPushMatrix();
			glTranslatef(0.72,0,0);
			caja(0.06,0.6,0.58);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(-0.72,0,0);
			caja(0.06,0.6,0.58);
		glPopMatrix();
		// Sumidero
		glPushMatrix();
			glTranslatef(0.345,0.1,0);
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[negro]);
			falsoCilindro(0.01,0.025);
		glPopMatrix();
		glTranslatef(-0.72,0.6,0);
		// Grifo
		glPushMatrix();
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[gris]);
			glRotatef(-90,1,0,0);
			creaManivela();
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0,0,0.08);
			falsoCilindro(0.05,0.02);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0,0,-0.08);
			falsoCilindro(0.05,0.02);
		glPopMatrix();
	glPopMatrix();
}

void creaWC()
{
	glPushMatrix();
		glTranslatef(-0.075,0,0);
		caja(0.4,0.3,0.3); // Base
		glTranslatef(0.075,0.3,0);
		caja(0.55,0.1,0.4); // Cuerpo
		glTranslatef(0,0.1,0);
		glPushMatrix();
			glTranslatef(0.075,0,0);
			caja(0.3,0.02,0.3); // Tapa
		glPopMatrix();
		glTranslatef(-0.2,0,0);
		caja(0.15,0.4,0.4); // Cisterna
		glTranslatef(0,0.4,0);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[gris]);
		falsoCilindro(0.03,0.02); // Cadena
	glPopMatrix();
}

void creaLavabo()
{
	glPushMatrix();
		glPushMatrix();
			glTranslatef(-0.1,0,0);
			caja(0.2,0.7,0.2); // Pie
		glPopMatrix();
		glTranslatef(0,0.7,0);
		glPushMatrix();
			glTranslatef(0.06,0,0);
			caja(0.37,0.05,0.64); // Base
		glPopMatrix();
		// Laterales
		glPushMatrix();
			glTranslatef(0,0,-0.335);
			caja(0.55,0.15,0.03);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0,0,0.335);
			caja(0.55,0.15,0.03);
		glPopMatrix();
		// Frontal y trasera
		glPushMatrix();
			glTranslatef(0.26,0,0);
			caja(0.03,0.15,0.64);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(-0.2,0,0);
			caja(0.15,0.15,0.64);
			glTranslatef(0.03,0.15,0);
			// Grifo
			glPushMatrix();
				glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[gris]);
				glRotatef(-90,1,0,0);
				creaManivela();
			glPopMatrix();
			glPushMatrix();
				glTranslatef(0,0,0.08);
				falsoCilindro(0.04,0.01);
			glPopMatrix();
			glPushMatrix();
				glTranslatef(0,0,-0.08);
				falsoCilindro(0.04,0.01);
			glPopMatrix();
		glPopMatrix();
		// Sumidero
		glPushMatrix();
			glTranslatef(0,0.05,0);
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[negro]);
			falsoCilindro(0.005,0.01);
		glPopMatrix();
	glPopMatrix();
}

void creaArmario(int idcolor)
{
	glPushMatrix();
		glRotatef(90,0,1,0);
		glTranslatef(0,0,-0.0275);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[idcolor]);
		caja(1,2,0.6); // Cuerpo
		// Cajones
		glTranslatef(0,0.05,0.3);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[idcolor]);
		creaCajon(2,0.17,0.96);
		glTranslatef(0,0.18,0);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[idcolor]);
		creaCajon(2,0.17,0.96);
		glTranslatef(0,0.18,RELIEVECAJONMITAD);
		// Puertas
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[idcolor]);
		glPushMatrix();
			glTranslatef(-0.24125,0,0);
			caja(0.4775,1.54,RELIEVECAJON);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0.24125,0,0);
			caja(0.4775,1.54,RELIEVECAJON);
		glPopMatrix();
		glTranslatef(0,0.77,0);
		// Tiradores
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[gris]);
		glPushMatrix();
			glTranslatef(0.0775,0,0);
			glRotatef(90,1,0,0);
			falsoCilindro(0.025,0.0125);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(-0.0775,0,0);
			glRotatef(90,1,0,0);
			falsoCilindro(0.025,0.0125);
		glPopMatrix();
	glPopMatrix();
}

void creaMesitaNoche(int idcolor)
{
	glPushMatrix();
		glRotatef(90,0,1,0);
		glTranslatef(0,0,-0.0275);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[idcolor]);
		caja(0.5,0.53,0.27); // Cuerpo
		glPushMatrix();
			glTranslatef(0,0.53,0.015);
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[idcolor]);
			caja(0.53,0.02,0.3); // Tablero
		glPopMatrix();
		// Cajones
		glTranslatef(0,0.05,0.135);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[idcolor]);
		creaCajon(2,0.15,0.5);
		glTranslatef(0,0.16,0);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[idcolor]);
		creaCajon(2,0.15,0.5);
		glTranslatef(0,0.16,0);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[idcolor]);
		creaCajon(2,0.15,0.5);
	glPopMatrix();
}

void creaCajon(int num_tir,float alto,float ancho)
{
	if(num_tir == 1 || num_tir == 2){
		glPushMatrix();
			glTranslatef(0,0,RELIEVECAJONMITAD);
			caja(ancho,alto,RELIEVECAJON);
			glTranslatef(0,(alto/2)-0.0125,RELIEVECAJONMITAD);
			// Tiradores
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[gris]);
			if(num_tir == 1){
				glRotatef(90,1,0,0);
				falsoCilindro(0.025,0.0125);
			}
			else{
				glPushMatrix();
					glTranslatef((ancho/2)-(ancho*0.19),0,0);
					glRotatef(90,1,0,0);
					falsoCilindro(0.025,0.0125);
				glPopMatrix();
				glPushMatrix();
					glTranslatef(-((ancho/2)-(ancho*0.19)),0,0);
					glRotatef(90,1,0,0);
					falsoCilindro(0.025,0.0125);
				glPopMatrix();
			}
		glPopMatrix();
	}
}

void creaCajonera(int idcolor)
{
	glPushMatrix();
		glRotatef(90,0,1,0);
		glTranslatef(0,0,-0.0275);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[idcolor]);
		caja(0.5,0.85,0.6); // Cuerpo
		glPushMatrix();
			glTranslatef(0,0.85,0.015);
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[idcolor]);
			caja(0.5,0.02,0.655); // Tablero
		glPopMatrix();
		// Cajones
		glTranslatef(0,0.05,0.3);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[idcolor]);
		creaCajon(2,0.15,0.49);
		glTranslatef(0,0.16,0);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[idcolor]);
		creaCajon(2,0.15,0.49);
		glTranslatef(0,0.16,0);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[idcolor]);
		creaCajon(2,0.15,0.49);
		glTranslatef(0,0.16,0);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[idcolor]);
		creaCajon(2,0.15,0.49);
		glTranslatef(0,0.16,0);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[idcolor]);
		creaCajon(2,0.15,0.49);
	glPopMatrix();
}

void creaEncimera(int idcolor)
{
	glPushMatrix();
		glRotatef(90,0,1,0);
		glTranslatef(0,0,-0.0275);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[idcolor]);
		caja(1,0.85,0.6); // Cuerpo
		glPushMatrix();
			glTranslatef(0,0.85,0.015);
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[idcolor]);
			caja(1,0.02,0.655); // Tablero
		glPopMatrix();
		glTranslatef(0,0.05,0.3);
		// Puertas
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[idcolor]);
		glPushMatrix();
			glTranslatef(-0.24125,0,0);
			caja(0.4775,0.79,RELIEVECAJON);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0.24125,0,0);
			caja(0.4775,0.79,RELIEVECAJON);
		glPopMatrix();
		glTranslatef(0,0.7,0);
		// Tiradores
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[gris]);
		glPushMatrix();
			glTranslatef(0.0775,0,0);
			glRotatef(90,1,0,0);
			falsoCilindro(0.025,0.0125);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(-0.0775,0,0);
			glRotatef(90,1,0,0);
			falsoCilindro(0.025,0.0125);
		glPopMatrix();
	glPopMatrix();
}

void creaHornilla(int idcolor)
{
	creaEncimera(idcolor);
	glPushMatrix();
		glRotatef(90,0,1,0);
		glTranslatef(0,0.87,0);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[negro]);
		caja(0.7,0.01,0.55);
		glTranslatef(0,0.01,0);
		// Fuegos
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[rojo]);
		glPushMatrix();
			glTranslatef(0.15,0,0.125);
			caja(0.15,0.01,0.15);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(-0.15,0,0.125);
			caja(0.15,0.01,0.15);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0.15,0,-0.125);
			caja(0.15,0.01,0.15);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(-0.15,0,-0.125);
			caja(0.15,0.01,0.15);
		glPopMatrix();
	glPopMatrix();
}

void creaVentana1()
{
	glPushMatrix();
		glRotatef(90,0,1,0);
		glTranslatef(0,1.05,0);
		caja(0.55,0.1,0.6); // Base
		glPushMatrix();
			glTranslatef(-0.295,0,0);
			caja(0.1,1.2,0.6); // Lateral izquierdo
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0.295,0,0);
			caja(0.1,1.2,0.6); // Lateral derecho
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0,1.1,0);
			caja(0.55,0.1,0.6); // Tapa
		glPopMatrix();
		// Manivela
		glPushMatrix();
			glTranslatef(-0.295,0.6,0.3);
			glRotatef(-90,0,0,1);
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[negro]);
			creaManivela();
		glPopMatrix();
		// Cristales
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[celeste]);
		glTranslatef(0,0.1,0);
		caja(0.55,1,0.52);
	glPopMatrix();
}

void creaVentana2()
{
	glPushMatrix();
		glRotatef(90,0,1,0);
		glTranslatef(0,1.05,0);
		glPushMatrix();
			glTranslatef(-0.33,0,0);
			caja(0.65,0.1,0.6); // Base izquierda
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0.33,0,0);
			caja(0.65,0.1,0.6); // Base derecha
		glPopMatrix();
		glPushMatrix();
			glTranslatef(-0.705,0,0);
			caja(0.1,1.2,0.6); // Lateral izquierdo
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0.705,0,0);
			caja(0.1,1.2,0.6); // Lateral derecho
		glPopMatrix();
		glPushMatrix();
			glTranslatef(-0.055,0.1,0);
			caja(0.1,1,0.6); // Centro izquierda
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0.055,0.1,0);
			caja(0.1,1,0.6); // Centro derecha
		glPopMatrix();
		glPushMatrix();
			glTranslatef(-0.33,1.1,0);
			caja(0.65,0.1,0.6); // Tapa izquierda
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0.33,1.1,0);
			caja(0.65,0.1,0.6); // Tapa izquierda
		glPopMatrix();
		caja(0.01,1.2,0.52);
		// Manivela izquierda
		glPushMatrix();
			glTranslatef(-0.055,0.6,0.3);
			glRotatef(-90,0,0,1);
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[negro]);
			creaManivela();
		glPopMatrix();
		// Manivela derecha
		glPushMatrix();
			glTranslatef(0.055,0.6,0.3);
			glRotatef(-90,0,0,1);
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[negro]);
			creaManivela();
		glPopMatrix();
		// Cristales
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[celeste]);
		glTranslatef(0,0.1,0);
		glPushMatrix();
			glTranslatef(-0.38,0,0);
			caja(0.55,1,0.52);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0.38,0,0);
			caja(0.55,1,0.52);
		glPopMatrix();
	glPopMatrix();
}

void creaPuerta1()
{
	glPushMatrix();
		// Puerta
		caja(0.55,2.05,0.83);
		// Marco
		glPushMatrix();
			glTranslatef(0,0,-0.475);
			caja(0.65,2.17,0.12);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0,0,0.475);
			caja(0.65,2.17,0.12);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0,2.05,0);
			caja(0.65,0.12,0.83);
		glPopMatrix();
		// Mirilla
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[gris]);
		glPushMatrix();
			glTranslatef(0,1.6,0);
			caja(0.56,0.05,0.05);
		glPopMatrix();
		// Cerradura
		glPushMatrix();
			glTranslatef(0,1,-0.335);
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[negro]);
			caja(0.56,0.05,0.05);
		glPopMatrix();
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[gris]);
		// Manivela interior
		glPushMatrix();
			glTranslatef(0.275,1.1,-0.335);
			glRotatef(-90,0,1,0);
			glRotatef(180,1,0,0);
			creaManivela();
		glPopMatrix();
		// Tirador gordo
		glPushMatrix();
			glTranslatef(-0.3,1,0);
			caja(0.05,0.05,0.05);
			glTranslatef(-0.05,-0.025,0);
			caja(0.05,0.1,0.1);
		glPopMatrix();
	glPopMatrix();
}

void creaPuerta2()
{
	glPushMatrix();
		caja(0.15,2,0.9);
		// Marco
		glPushMatrix();
			glTranslatef(0,0,-0.475);
			caja(0.2,2.1,0.05);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0,0,0.475);
			caja(0.2,2.1,0.05);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0,2,0);
			caja(0.2,0.1,0.9);
		glPopMatrix();
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[gris]);
		// Manivelas
		glPushMatrix();
			glTranslatef(-0.075,1.1,0.35);
			glRotatef(90,0,1,0);
			glRotatef(180,1,0,0);
			creaManivela();
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0.075,1.1,0.35);
			glRotatef(90,0,1,0);
			creaManivela();
		glPopMatrix();
	glPopMatrix();
}

/**
	Comprueba si la posici�n x_pos,z_pos est� a menos de DISTVALIDA de alg�n muro,
	en caso negativo devolver� -1,
	en caso afirmativo devolver� el identificador del vector de muros del muro asociado 
	y actualizar� las variables x,z que contendr�n la posici�n dentro del muro
**/
int cercaMuro(float x_pos,float z_pos,float * x_cor,float * z_cor,float sep_borde)
{
	int k,analizados,muro_mejor = -1;
	float res_analisis,x_aux,z_aux,dist_mejor = 1000000;
	
	analizados = 0;
	k=0;
	while(analizados < num_muros_conjunto){
		if(conjunto_muros[k].libre == 0){
			// Analizamos para el muro k
			res_analisis = analizar(x_pos,z_pos,&x_aux,&z_aux,k,sep_borde);
			if(res_analisis != -1){
				if(res_analisis < dist_mejor){
					muro_mejor = k;
					*x_cor = x_aux;
					*z_cor = z_aux;
				}
			}
			++analizados;
		}
		++k;
	}
	return muro_mejor;
}

/**
	Funci�n de an�lisis para la funci�n anterior, aqui se le pasa el muro a analizar
	Devuelve -1, si no est� a menos de DISTVALIDA y la distancia en caso afirmativo
	Hace la actualizaci�n de variables descrita en la funci�n anterior
**/
float analizar(float x_pos,float z_pos,float * x_cor,float * z_cor,int idmuro,float sep_borde)
{
	float res = -1;
	float A,B,C; // Ecuaci�n general de la recta Ax + By + C = 0
	float x1 = conjunto_muros[idmuro].muro.puntos[0].x;
	float y1 = conjunto_muros[idmuro].muro.puntos[0].y;
	float x2 = conjunto_muros[idmuro].muro.puntos[1].x;
	float y2 = conjunto_muros[idmuro].muro.puntos[1].y;
	
	float x3 = x_pos;
	float y3 = z_pos;
	
	// Calcula la recta que pasa por los puntos del muro
	A = (y2 - y1)/(x2 - x1);
	B = -1;
	C = (((-x1) * y2)+(x2 * y1))/(x2 - x1);
	
	// Distancia a la recta
	
	float dist = abs(A*x3+B*y3+C)/(sqrt(A*A+B*B));
	
	if(dist < DISTVALIDA){
		
		/*
		Calcular punto sobre la recta en la perpendicular
			Para ello:
				Calcular recta perpendicular que pase por el punto x3,y3
					Para ello:
						Calcular pendiente de nuestra recta Ax + By + C = 0*/
		float m = (-A)/B;
						/*Calcular pendiente de la nueva recta, pendiente nueva = - 1 / pendiente antigua*/
		float m_nueva = (-1) / m;
						/*Calcular la ecuaci�n general de la nueva recta*/
		float A2,B2,C2; // Ecuaci�n general de la recta Ax + By + C = 0
		float abcisa = y3 - m_nueva * x3;
		A2 = -m_nueva;
		B2 = 1;
		C2 = -abcisa;
				/*Hallar el punto de corte de las 2 rectas
					Para ello:
						Resolver el sistema de ecuaciones formado por las 2 ecuaciones generales
						A  x + B  y + C  =0
						A2 x + B2 y + C2 =0
						
						De la primera despejamos x
						
						x = ((-B*y)-C)/A
						y = (((C*A2)/A)-C2)/(((-B*A2)/A)+B2)
						*/
		float y = (((C*A2)/A)-C2)/(((-B*A2)/A)+B2);
		float x = ((-B*y)-C)/A;
		
		// Ahora hay que comprobar que est� entre los 2 puntos del muro
		float tam_muro = distanciaEuclidea2D(x1,y1,x2,y2);
		tam_muro -= sep_borde;
		if((distanciaEuclidea2D(x1,y1,x,y) <= tam_muro)&&(distanciaEuclidea2D(x2,y2,x,y) <= tam_muro)){
			res = dist;
			*x_cor = x;
			*z_cor = y;
		}
	}
	return res;
}

void creaManivela()
{
	glPushMatrix();
		glTranslatef(0,-0.0125,0.02);
		caja(0.01,0.025,0.04); // Eje
		glTranslatef(0.055,0,0.025);
		caja(0.12,0.025,0.01); // Maneta
	glPopMatrix();
}



