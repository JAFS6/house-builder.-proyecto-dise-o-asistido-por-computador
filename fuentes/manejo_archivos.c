#include "grua.h"

/**
	Gestiona el proceso de guardado
**/
void guardado()
{
	if(opcion_guardar == 1 || strlen(save_file) <= 0){
		printf("\nNombre del fichero a guardar: ");
		gets(save_file);
		strcat(save_file,".hb");
	}
	
	if(guardar(save_file) == -1){
		printf("\nError al intentar guardar el fichero %s\n",save_file);
	}
	else{
		printf("\n%s guardado correctamente.\n",save_file);
		gluiOutput("Guardado completado");
	}
}

/**
	Guarda la escena en un archivo
	fich: El nombre del fichero
	Devuelve 0 en caso de �xito y -1 en caso de fallo
**/
int guardar(char * fich)
{
	int error = 0;
	FILE * fd_fichero;
	char buffer[TAM_MAX_RUTA];
	// Abrimos el fichero
	fd_fichero = fopen(fich, "w");
	if (fd_fichero == NULL){
		error = -1;
	}
	else{ // Guardamos la escena
		int n,k;
		// N�mero de muebles
		n = sprintf(buffer,"%i\n",num_muebles_conjunto);
		fwrite(buffer,1,n,fd_fichero);
		// Guardamos los par�metros de cada mueble
		for(k=0; k<MAXMUEBLES; ++k){
			if(conjunto_muebles[k].libre == 0){
				n = sprintf(buffer,"%i\n%f\n%f\n%f\n%i\n%f\n",
							conjunto_muebles[k].mueble.tipo,
							conjunto_muebles[k].mueble.x,
							conjunto_muebles[k].mueble.y,
							conjunto_muebles[k].mueble.z,
							conjunto_muebles[k].mueble.color,
							conjunto_muebles[k].mueble.rot);
				fwrite(buffer,1,n,fd_fichero);
			}
		}
		// N�mero de muros
		n = sprintf(buffer,"%i\n",num_muros_conjunto);
		fwrite(buffer,1,n,fd_fichero);
		// Guardamos los par�metros de cada muro
		for(k=0; k<MAXMUROS; ++k){
			if(conjunto_muros[k].libre == 0){
				n = sprintf(buffer,"%i\n%f\n%i\n%f\n%f\n%f\n%f\n",
							conjunto_muros[k].muro.tipo,
							conjunto_muros[k].muro.grosor,
							conjunto_muros[k].muro.color,
							conjunto_muros[k].muro.puntos[0].x,
							conjunto_muros[k].muro.puntos[0].y,
							conjunto_muros[k].muro.puntos[1].x,
							conjunto_muros[k].muro.puntos[1].y);
				fwrite(buffer,1,n,fd_fichero);
			}
		}
		// N�mero de elementos
		n = sprintf(buffer,"%i\n",num_elementos_conjunto);
		fwrite(buffer,1,n,fd_fichero);
		// Guardamos los par�metros de cada elemento
		for(k=0; k<MAXMELEMENTOS; ++k){
			if(conjunto_elementos[k].libre == 0){
				n = sprintf(buffer,"%i\n%f\n%f\n%f\n%i\n%f\n%i\n",
							conjunto_elementos[k].elemento.tipo,
							conjunto_elementos[k].elemento.x,
							conjunto_elementos[k].elemento.y,
							conjunto_elementos[k].elemento.z,
							conjunto_elementos[k].elemento.color,
							conjunto_elementos[k].elemento.rot,
							conjunto_elementos[k].elemento.cara);
				fwrite(buffer,1,n,fd_fichero);
			}
		}
		// Cerramos el fichero
		fclose(fd_fichero);
	}
	return error;
}

/**
	Gestiona el proceso de cargado
**/
void cargado()
{
	char buffer[TAM_MAX_RUTA];
	int seguir = -1; // -1 Opcion incorrecta, 0 no, 1 si
	printf("\nSe perderan los cambios que no se hayan guardado\nContinuar? (S/N): ");
	gets(buffer);
	if(strlen(buffer) == 1){
		if(toupper(buffer[0]) == 'S'){
			seguir = 1;
		}
		else if(toupper(buffer[0]) == 'N'){
			seguir = 0;
		}
	}
	while(seguir == -1){
		printf("\nOpcion incorrecta\nContinuar? (S/N): ");
		gets(buffer);
		if(strlen(buffer) == 1){
			if(toupper(buffer[0]) == 'S'){
				seguir = 1;
			}
			else if(toupper(buffer[0]) == 'N'){
				seguir = 0;
			}
		}
	}
	
	if(seguir == 1){ // Se ha elegido abrir otra escena
		char save_file_aux[TAM_MAX_RUTA];
		printf("\nNombre del fichero a abrir (sin extension): ");
		gets(save_file_aux);
		strcat(save_file_aux,".hb");
		
		if(cargar(save_file_aux) == -1){
			printf("\nError al intentar abrir el fichero %s\n",save_file_aux);
			gluiOutput("Error al cargar");
		}
		else{
			estado = seleccionar;
			estado_menu = 0;
			strcpy(save_file,save_file_aux);
			printf("\n%s cargado correctamente.\n",save_file);
			gluiOutput("Carga completada");
		}
	}
}

/**
	Carga la escena desde un archivo con extensi�n .hb
	fich: El nombre del fichero
	Devuelve 0 en caso de �xito y -1 en caso de fallo
**/
int cargar(char * fich)
{
	int error = 0;
	FILE * fd_fichero;
	char buffer[TAM_MAX_RUTA];
	// Abrimos el fichero
	fd_fichero = fopen(fich, "r");
	if (fd_fichero == NULL){
		error = -1;
	}
	else{ // Leemos la escena
		inicializa_conj_muebles();
		inicializa_conj_muros();
		int n,k,l;
		MUEBLE m;
		fgets(buffer,TAM_MAX_RUTA,fd_fichero);
		// N�mero de muebles
		n = atoi(buffer);
		// Leemos los par�metros de cada mueble
		for(k=0; k<n; ++k){
			fgets(buffer,TAM_MAX_RUTA,fd_fichero);
			m.tipo = atoi(buffer);
			fgets(buffer,TAM_MAX_RUTA,fd_fichero);
			m.x = atof(buffer);
			fgets(buffer,TAM_MAX_RUTA,fd_fichero);
			m.y = atof(buffer);
			fgets(buffer,TAM_MAX_RUTA,fd_fichero);
			m.z = atof(buffer);
			fgets(buffer,TAM_MAX_RUTA,fd_fichero);
			m.color = atoi(buffer);
			fgets(buffer,TAM_MAX_RUTA,fd_fichero);
			m.rot = atof(buffer);
			anadir_mueble(&m);
		}
		MURO mu;
		fgets(buffer,TAM_MAX_RUTA,fd_fichero);
		// N�mero de muros
		n = atoi(buffer);
		// Leemos los par�metros de cada muro
		for(k=0; k<n; ++k){
			fgets(buffer,TAM_MAX_RUTA,fd_fichero);
			mu.tipo = atoi(buffer); // Tipo
			fgets(buffer,TAM_MAX_RUTA,fd_fichero);
			mu.grosor = atof(buffer); // Grosor
			fgets(buffer,TAM_MAX_RUTA,fd_fichero);
			mu.color = atoi(buffer); // Color
			fgets(buffer,TAM_MAX_RUTA,fd_fichero);
			mu.puntos[0].x = atof(buffer);
			fgets(buffer,TAM_MAX_RUTA,fd_fichero);
			mu.puntos[0].y = atof(buffer);
			fgets(buffer,TAM_MAX_RUTA,fd_fichero);
			mu.puntos[1].x = atof(buffer);
			fgets(buffer,TAM_MAX_RUTA,fd_fichero);
			mu.puntos[1].y = atof(buffer);
			anadir_muro(&mu);
		}
		ELEMENTO e;
		fgets(buffer,TAM_MAX_RUTA,fd_fichero);
		// N�mero de elementos
		n = atoi(buffer);
		// Leemos los par�metros de cada elemento
		for(k=0; k<n; ++k){
			fgets(buffer,TAM_MAX_RUTA,fd_fichero);
			e.tipo = atoi(buffer);
			fgets(buffer,TAM_MAX_RUTA,fd_fichero);
			e.x = atof(buffer);
			fgets(buffer,TAM_MAX_RUTA,fd_fichero);
			e.y = atof(buffer);
			fgets(buffer,TAM_MAX_RUTA,fd_fichero);
			e.z = atof(buffer);
			fgets(buffer,TAM_MAX_RUTA,fd_fichero);
			e.color = atoi(buffer);
			fgets(buffer,TAM_MAX_RUTA,fd_fichero);
			e.rot = atof(buffer);
			fgets(buffer,TAM_MAX_RUTA,fd_fichero);
			e.cara = atoi(buffer);
			anadir_elemento(&e);
		}
		// Cerramos el fichero
		fclose(fd_fichero);
	}
	return error;
}

/**
	Prepara una nueva escena
**/
void nueva_escena()
{
	char buffer[TAM_MAX_RUTA];
	int seguir = -1; // -1 Opcion incorrecta, 0 no, 1 si
	printf("\nSe perderan los cambios que no se hayan guardado\nContinuar? (S/N): ");
	gets(buffer);
	if(strlen(buffer) == 1){
		if(toupper(buffer[0]) == 'S'){
			seguir = 1;
		}
		else if(toupper(buffer[0]) == 'N'){
			seguir = 0;
		}
	}
	while(seguir == -1){
		printf("\nOpcion incorrecta\nContinuar? (S/N): ");
		gets(buffer);
		if(strlen(buffer) == 1){
			if(toupper(buffer[0]) == 'S'){
				seguir = 1;
			}
			else if(toupper(buffer[0]) == 'N'){
				seguir = 0;
			}
		}
	}
	
	if(seguir == 1){ // Se ha elegido obtener escena nueva
		save_file[0] = '\0';
		inicializa_conj_muebles();
		inicializa_conj_muros();
		inicializa_conj_elementos();
		inicializaPrograma();
	}
	gluiOutput("Nueva escena lista");
}

void salida()
{
	char buffer[TAM_MAX_RUTA];
	int seguir = -1; // -1 Opcion incorrecta, 0 no, 1 si
	printf("\nSeguro que quiere salir? (S/N): ");
	gets(buffer);
	if(strlen(buffer) == 1){
		if(toupper(buffer[0]) == 'S'){
			seguir = 1;
		}
		else if(toupper(buffer[0]) == 'N'){
			seguir = 0;
		}
	}
	while(seguir == -1){
		printf("\nOpcion incorrecta\nSalir? (S/N): ");
		gets(buffer);
		if(strlen(buffer) == 1){
			if(toupper(buffer[0]) == 'S'){
				seguir = 1;
			}
			else if(toupper(buffer[0]) == 'N'){
				seguir = 0;
			}
		}
	}
	if(seguir == 1){
		exit(0);      // Salir del programa
	}
}

