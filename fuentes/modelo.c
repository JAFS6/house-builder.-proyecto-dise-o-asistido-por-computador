/*	Grua 

	C.A.D. 						Curso 2008-2009
 	
	Codigo base para la realización de las practicas de CAD

	modulo modelo.c
	Dibujo del modelo
=======================================================
	J.C. Torres 
	Dpto. Lenguajes y Sistemas Informticos
	(Univ. de Granada, SPAIN)

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details 
 http://www.gnu.org/copyleft/gpl.html

=======================================================
Queda prohibido cobrar canon por la copia de este software

*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>                   // Libreria de utilidades de OpenGL

#define __modelo__			// Hace que se asigne memoria a las variables globales

#include "grua.h"

int COLORGRUA=0;


/**	void initModel()

Inicializa el modelo y de las variables globales

**/

void initModel()
{
	/**
		Definicion de los colores usados.
	**/
	float colores[11][4]={{1,1,0,1.}, /*Amarillo*/
						  {0.7,0.7,0.7,1}, /*Gris*/
						  {1.0,0.3,0.3,1}, /*Rojo*/
						  {0.7,0.6,0.2,1}, /*Marron*/
						  {0.2,1.0,1.0,1}, /*Celeste*/
						  {1.0,0.86,0.3,1}, /*Naranja*/
						  {1,1,1,1.}, /*Blanco*/
						  {1,0.6,1,1.}, /*Violeta*/
						  {0,0,1,1.}, /*Azul*/
						  {0.4,1,0.4,1.}, /*Verde*/
						  {0,0,0,1}}; /*Negro*/
	int i,j;
	for(i=0;i<4;++i)
		for(j=0;j<11;++j)
			color[j][i]=colores[j][i];

	COLORGRUA=0;
	colorActivo = marron;

	//*******
	// INICIALIZACIÓN DE LOS PARÁMETROS DE LA CÁMARA
	//*******
	view_rotx=30.0;	// Angulos de rotacion 
	view_roty=-45.0;
	view_rotz=0.0;
	d=100.0;
	
	x_camara=20;		// Posicion de la camara
	y_camara=2;
	z_camara=20;

	VISTA_PERSPECTIVA=1;	// Flag perspectiva/paralela

	ventanaMundoParalela=200;	// Ventana para proyeccion paralela
	origenXVentanaMundoParalelo=0;	
	origenYVentanaMundoParalelo=0;

	//*******
	// INICIALIZACIÓN DEL CONJUNTO DE CAJAS
	//*******
	num_cajas_conjunto = -1;
	objetoSeleccionado = -1;
	colgado = -1;
	//*******
	// INICIALIZACIÓN DEL CONJUNTO DE MUEBLES
	//*******
	// Tamaño máximo del sillón
	matriz_max_mueble[0].x = 1;
	matriz_max_mueble[0].y = 1;
	matriz_max_mueble[0].z = 1.1;
	// Tamaño máximo del sofa
	matriz_max_mueble[1].x = 1;
	matriz_max_mueble[1].y = 1;
	matriz_max_mueble[1].z = 2.1;
	// Tamaño máximo del frigorífico
	matriz_max_mueble[2].x = 1.1;
	matriz_max_mueble[2].y = 2;
	matriz_max_mueble[2].z = 1;
	// Tamaño máximo de la mesa pequeña
	matriz_max_mueble[3].x = 1;
	matriz_max_mueble[3].y = 0.45;
	matriz_max_mueble[3].z = 1;
	// Tamaño máximo de la silla
	matriz_max_mueble[4].x = 0.4;
	matriz_max_mueble[4].y = 1;
	matriz_max_mueble[4].z = 0.4;
	// Tamaño máximo de la mesa grande
	matriz_max_mueble[5].x = 1.2;
	matriz_max_mueble[5].y = 0.85;
	matriz_max_mueble[5].z = 0.9;
	// Tamaño máximo de la estanteria grande
	matriz_max_mueble[6].x = 0.3;
	matriz_max_mueble[6].y = 2.2;
	matriz_max_mueble[6].z = 1.2;
	// Tamaño máximo de la cama
	matriz_max_mueble[7].x = 2.1;
	matriz_max_mueble[7].y = 0.67;
	matriz_max_mueble[7].z = 1.1;
	// Tamaño máximo de la bañera
	matriz_max_mueble[8].x = 1.5;
	matriz_max_mueble[8].y = 0.65;
	matriz_max_mueble[8].z = 0.7;
	// Tamaño máximo de la WC
	matriz_max_mueble[9].x = 0.55;
	matriz_max_mueble[9].y = 0.83;
	matriz_max_mueble[9].z = 0.4;
	// Tamaño máximo del lavabo
	matriz_max_mueble[10].x = 0.55;
	matriz_max_mueble[10].y = 0.9;
	matriz_max_mueble[10].z = 0.7;
	// Tamaño máximo de la mesita de noche
	matriz_max_mueble[11].x = 0.325;
	matriz_max_mueble[11].y = 0.55;
	matriz_max_mueble[11].z = 0.53;
	// Tamaño máximo del armario
	matriz_max_mueble[12].x = 0.655;
	matriz_max_mueble[12].y = 2;
	matriz_max_mueble[12].z = 1;
	// Tamaño máximo de la cajonera de cocina
	matriz_max_mueble[13].x = 0.655;
	matriz_max_mueble[13].y = 0.87;
	matriz_max_mueble[13].z = 0.5;
	// Tamaño máximo del armario de cocina
	matriz_max_mueble[14].x = 0.655;
	matriz_max_mueble[14].y = 0.87;
	matriz_max_mueble[14].z = 1;
	// Tamaño máximo de la hornilla
	matriz_max_mueble[15].x = 0.655;
	matriz_max_mueble[15].y = 0.89;
	matriz_max_mueble[15].z = 1;
	
	inicializa_conj_muebles();
	//*******
	// INICIALIZACIÓN DEL CONJUNTO DE ELEMENTOS MURALES
	//*******
	inicializa_conj_elementos();
	// Tamaño máximo de la ventana de 1 hoja
	matriz_max_elemento[0].x = 0.6;
	matriz_max_elemento[0].y = 1.2;
	matriz_max_elemento[0].z = 0.75;
	// Tamaño máximo de la ventana de 2 hojas
	matriz_max_elemento[1].x = 0.6;
	matriz_max_elemento[1].y = 1.2;
	matriz_max_elemento[1].z = 1.51;
	// Tamaño máximo de la puerta de muro exterior
	matriz_max_elemento[2].x = 0.65;
	matriz_max_elemento[2].y = 2.17;
	matriz_max_elemento[2].z = 1.07;
	// Tamaño máximo de la puerta de muro interior
	matriz_max_elemento[3].x = 0.2;
	matriz_max_elemento[3].y = 2.1;
	matriz_max_elemento[3].z = 1;
	// Total 2.1 x 0.9
	//*******
	// INICIALIZACIÓN DEL CONJUNTO DE MUROS
	//*******
	inicializa_conj_muros();
	muro_oculto = 0;
	punto_mover = 0;
	//*******
	// INICIALIZACIÓN DEL ESTADO DEL PROGRAMA
	//*******
	inicializaPrograma();
	//*******
	// INICIALIZACIÓN GESTIÓN DE FICHEROS
	//*******
	strcpy(save_file,"");
}

void inicializaPrograma()
{
	estado = seleccionar;
	estado_menu = 0;
	estado_ejes = 1;
	estado_cuadricula = 0;
	estado_rotacion = 0;
}

/**	void Dibuja( void )

Procedimiento de dibujo del modelo. Es llamado por glut cada vez que se debe redibujar.

**/

void Dibuja( void )
{
	static GLfloat pos[4] = {5.0, 5.0, 10.0, 0.0 };		// Posicion de la fuente de luz
	int i;

	glPushMatrix();		// Apila la transformacion geometrica actual

		glClearColor(0,0,0.6,1);	// Fija el color de fondo a azul
		
		glClear( GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT );	 // Inicializa el buffer de color

		transformacionVisualizacion();	 // Carga transformacion de visualizacion

		glLightfv( GL_LIGHT0, GL_POSITION, pos );	// Declaracion de luz. Colocada aqui esta fija en la escena
		
		if(estado_ejes == 1){
			ejes(3); // Dibuja los ejes
		}
		
		if(estado_cuadricula == 1){
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[negro]);
			cuadricula(); // Dibuja la cuadricula
		}
		// Dibuja el suelo
		glPushMatrix();
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[verde]);
			glTranslatef(0,-0.5,0);
			caja(200,0.5,200);
		glPopMatrix();
		
		int k;
		/* Dibuja el conjunto de cajas
		for(k=0; k<=num_cajas_conjunto; k++){
			if(colgado != k){
				glPushMatrix();
					glPushName(k);
					glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[conjunto_cajas[k].color]);
					glTranslatef(conjunto_cajas[k].x,conjunto_cajas[k].y,conjunto_cajas[k].z);
					glRotatef(conjunto_cajas[k].rot,0,1,0);
					caja(conjunto_cajas[k].tam_x,conjunto_cajas[k].tam_y,conjunto_cajas[k].tam_z);
					glPopName();
				glPopMatrix();
			}
		}*/
		
		// Dibuja el mueble que se está colocando
		if(estado == PoniendoMuebles && estado_anadir_mueble == 1){
			pintaMueble(&nuevo_mueble);
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[COLORSEL]);
			marcarSeleccion2(nuevo_mueble.x,nuevo_mueble.y,nuevo_mueble.z,nuevo_mueble.rot,
							matriz_max_mueble[(nuevo_mueble.tipo)].x,matriz_max_mueble[(nuevo_mueble.tipo)].y,matriz_max_mueble[(nuevo_mueble.tipo)].z);
		}
		
		// Dibuja el conjunto de muebles
		int pintados = 0;
		k=0;
		while(pintados < num_muebles_conjunto){
			if(conjunto_muebles[k].libre == 0){
				glPushName(NOMBRE_MUEBLE+k);
				pintaMueble(&(conjunto_muebles[k].mueble));
				glPopName();
				++pintados;
			}
			++k;
		}
		
		// Dibuja el muro que se está colocando
		if(estado == PoniendoMuros && estado_anadir_muro >= 1){
			if(punto_colocando == 1 && estado_anadir_muro == 3){
				pintaMuro(&nuevo_muro);
				glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[COLORSEL]);
				marcarMuro(&nuevo_muro);
			}
			else{
				glPushMatrix();
					glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[nuevo_muro.color]);
					glTranslatef(nuevo_muro.puntos[0].x,0,nuevo_muro.puntos[0].y);
					caja(nuevo_muro.grosor,alturaMuro(),nuevo_muro.grosor);
				glPopMatrix();
			}
		}
		
		// Dibuja el conjunto de muros
		pintados = 0;
		k=0;
		while(pintados < num_muros_conjunto){
			if(conjunto_muros[k].libre == 0){
				glPushName(NOMBRE_MURO+k);
				pintaMuro(&(conjunto_muros[k].muro));
				glPopName();
				++pintados;
			}
			++k;
		}
		
		// Dibuja el elemento que se está colocando
		if(estado == PoniendoElementos && pintar_muro == 1){
			pintaElemento(&nuevo_elemento);
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[COLORSEL]);
			glPushMatrix();
			if(nuevo_elemento.tipo == 0 || nuevo_elemento.tipo == 1){ // Solo para las ventanas hay que subir la seleccion
				glTranslatef(0,1.05,0);
			}
			marcarSeleccion2(nuevo_elemento.x,
							 nuevo_elemento.y,
							 nuevo_elemento.z,
							 nuevo_elemento.rot,
							 matriz_max_elemento[(nuevo_elemento.tipo)].x,
							 matriz_max_elemento[(nuevo_elemento.tipo)].y,
							 matriz_max_elemento[(nuevo_elemento.tipo)].z);
			glPopMatrix();
		}
		
		// Dibuja el conjunto de elementos
		pintados = 0;
		k=0;
		while(pintados < num_elementos_conjunto){
			if(conjunto_elementos[k].libre == 0){
				glPushName(NOMBRE_ELEMENTO+k);
				pintaElemento(&(conjunto_elementos[k].elemento));
				glPopName();
				++pintados;
			}
			++k;
		}
		
		marcarSeleccion(); // Marca el objeto seleccionado actualmente
		
	glPopMatrix(); 		// Desapila la transformacion geometrica

	glutSwapBuffers();	// Intercambia el buffer de dibujo y visualizacion
}

/**	void idle()

Procedimiento de fondo. Es llamado por glut cuando no hay eventos pendientes.

**/
void idle()
{
	actualizarModelo(); // Actualizar los parámetros del modelo
	gluiPostRedisplay(); // Redibuja
	glutTimerFunc(30,idle,0); // Vuelve a lanzar otro evento de redibujado en 30 ms
}

void marcarSeleccion()
{
	glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[COLORSEL]);
	
	if(objetoSeleccionado != -1){
		if(objetoSeleccionado >= NOMBRE_ELEMENTO){ // Elementos murales
			int aux = objetoSeleccionado-NOMBRE_ELEMENTO;
			glPushMatrix();
			if(conjunto_elementos[aux].elemento.tipo == 0 || conjunto_elementos[aux].elemento.tipo == 1){ // Solo para las ventanas hay que subir la seleccion
				glTranslatef(0,1.05,0);
			}
			marcarSeleccion2(conjunto_elementos[aux].elemento.x,
							 conjunto_elementos[aux].elemento.y,
							 conjunto_elementos[aux].elemento.z,
							 conjunto_elementos[aux].elemento.rot,
							 matriz_max_elemento[(conjunto_elementos[aux].elemento.tipo)].x,
							 matriz_max_elemento[(conjunto_elementos[aux].elemento.tipo)].y,
							 matriz_max_elemento[(conjunto_elementos[aux].elemento.tipo)].z);
			glPopMatrix();
		}
		else if(objetoSeleccionado >= NOMBRE_MURO){ // Muros
			marcarMuro(&(conjunto_muros[objetoSeleccionado-NOMBRE_MURO].muro));
		}
		else if(objetoSeleccionado >= NOMBRE_MUEBLE){ // Muebles
			int aux = objetoSeleccionado-NOMBRE_MUEBLE;
			marcarSeleccion2(conjunto_muebles[aux].mueble.x,
							 conjunto_muebles[aux].mueble.y,
							 conjunto_muebles[aux].mueble.z,
							 conjunto_muebles[aux].mueble.rot,
							 matriz_max_mueble[(conjunto_muebles[aux].mueble.tipo)].x,
							 matriz_max_mueble[(conjunto_muebles[aux].mueble.tipo)].y,
							 matriz_max_mueble[(conjunto_muebles[aux].mueble.tipo)].z);
		}
		else{ // Cajas
			marcarSeleccion2(conjunto_cajas[objetoSeleccionado].x,
							 conjunto_cajas[objetoSeleccionado].y,
							 conjunto_cajas[objetoSeleccionado].z,
							 conjunto_cajas[objetoSeleccionado].rot,
							 conjunto_cajas[objetoSeleccionado].tam_x,
							 conjunto_cajas[objetoSeleccionado].tam_y,
							 conjunto_cajas[objetoSeleccionado].tam_z);
		}
	}
}

void marcarMuro(MURO * m)
{
	marcarSeleccion2(((m->puntos[0].x)+(m->puntos[1].x))/2,
					 0,
					 ((m->puntos[0].y)+(m->puntos[1].y))/2,
					 angulo_orientacion(m->puntos[0].x,m->puntos[0].y,m->puntos[1].x,m->puntos[1].y),
					 distanciaEuclidea2D(m->puntos[0].x,m->puntos[0].y,m->puntos[1].x,m->puntos[1].y),
					 alturaMuro(),
					 m->grosor);
	if(estado == seleccionar){
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[rojo]);
		glPushMatrix();
			glTranslatef(m->puntos[punto_mover].x,alturaMuro(),m->puntos[punto_mover].y);
			caja(0.1,1,0.1);
		glPopMatrix();
	}
}

void marcarSeleccion2(float pos_x,float pos_y,float pos_z,float rot,float tam_x,float tam_y,float tam_z)
{
	int i;
	for(i=0; i<2; ++i){
		glPushMatrix();
			glTranslatef(pos_x,pos_y,pos_z);
			if(i==1)
				glRotatef(180,0,1,0);
			glRotatef(rot,0,1,0);
			glPushMatrix();
				glTranslatef(-((tam_x/2)+SEPSEL),-SEPSEL,-((tam_z/2)+SEPSEL));
				esquinaSeleccion(0,tam_x,tam_y,tam_z);
			glPopMatrix();
			glPushMatrix();
				glTranslatef(-((tam_x/2)+SEPSEL),tam_y+SEPSEL,((tam_z/2)+SEPSEL));
				glRotatef(180,1,0,0);
				esquinaSeleccion(0,tam_x,tam_y,tam_z);
			glPopMatrix();
			glPushMatrix();
				glTranslatef(-((tam_x/2)+SEPSEL),-SEPSEL,((tam_z/2)+SEPSEL));
				esquinaSeleccion(1,tam_x,tam_y,tam_z);
			glPopMatrix();
			glPushMatrix();
				glTranslatef(-((tam_x/2)+SEPSEL),tam_y+SEPSEL,-((tam_z/2)+SEPSEL));
				glRotatef(180,1,0,0);
				esquinaSeleccion(1,tam_x,tam_y,tam_z);
			glPopMatrix();
		glPopMatrix();
	}
}

void esquinaSeleccion(int pos,float tam_x,float tam_y,float tam_z)
{
	glPushMatrix();
		falsoCilindro(tam_y/4,0.005); // Eje Y
		glPushMatrix();
			glRotatef(270,0,0,1);
			falsoCilindro(tam_x/4,0.005); // Eje X
		glPopMatrix();
		glPushMatrix();
			if(pos == 0)
				glRotatef(90,1,0,0);
			else
				glRotatef(270,1,0,0);
			falsoCilindro(tam_z/4,0.005); // Eje Z
		glPopMatrix();
	glPopMatrix();
}

void enganchar()
{
	if(objetoSeleccionado != -1){
		colgado = objetoSeleccionado;
		objetoSeleccionado = -1;
		estado = seleccionar;
		gluiOutput("Caja enganchada");
	}
}

void desenganchar()
{
	// Calculos de la nueva posicion de la caja
	
	conjunto_cajas[colgado].x = (grua.posCarro+1.5) * cos(((grua.angY)*PI)/180);
	conjunto_cajas[colgado].z = -((grua.posCarro+1.5) * sin(((grua.angY)*PI)/180));
	
	apilar(colgado);
	conjunto_cajas[colgado].rot = grua.angY;
	colgado = -1;
	estado = seleccionar;
	gluiOutput("Caja desenganchada");
}

void actualizarModelo()
{
	// Orientación
	grua.angY+=grua.vangY;
	if(grua.angY>360) grua.angY-=360;
	if(grua.angY<0) grua.angY+=360;

	// Distribución
	if((grua.vposCarro > 0 && grua.posCarro < 26.8) || (grua.vposCarro < 0 && grua.posCarro > 4.2))
		grua.posCarro+=grua.vposCarro;

	// Elevación
	if((grua.vlCuerda > 0 && grua.lCuerda < 31.5) || (grua.vlCuerda < 0 && grua.lCuerda > 0.5))
		grua.lCuerda+=grua.vlCuerda;
}

void cuadricula()
{
	int i;
	for(i=0; i<((int)TAM_CUADRICULA)/2; ++i){
		glPushMatrix();
			glTranslatef(i,0,0);
			glRotatef(90,1,0,0);
			glTranslatef(0,-TAM_CUADRICULA/2,0);
			falsoCilindro(TAM_CUADRICULA,RAD_CUADRICULA);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(-i,0,0);
			glRotatef(90,1,0,0);
			glTranslatef(0,-TAM_CUADRICULA/2,0);
			falsoCilindro(TAM_CUADRICULA,RAD_CUADRICULA);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0,0,i);
			glRotatef(90,0,0,1);
			glTranslatef(0,-TAM_CUADRICULA/2,0);
			falsoCilindro(TAM_CUADRICULA,RAD_CUADRICULA);
		glPopMatrix();
		glPushMatrix();
			glTranslatef(0,0,-i);
			glRotatef(90,0,0,1);
			glTranslatef(0,-TAM_CUADRICULA/2,0);
			falsoCilindro(TAM_CUADRICULA,RAD_CUADRICULA);
		glPopMatrix();
	}
}

float grados2radianes(float angulo)
{
	return ((angulo * PI)/180.0);
}

float radianes2grados(float angulo)
{
	return ((angulo * 180)/PI);
}


