/*	Grua 

	C.A.D. 						Curso 2008-2009
 	
	Codigo base para la realización de las practicas de CAD

	modulo grua.h

=======================================================
	Copyright (c) J.C. Torres 
	Dpto. Lenguajes y Sistemas Informticos
	(Univ. de Granada, SPAIN)

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details 
 http://www.gnu.org/copyleft/gpl.html

=======================================================
Queda prohibido cobrar canon por la copia de este software

*/

// Includes
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>
#include <string.h>

#ifndef __gruaH__ // Para evitar doble inclusion
#define __gruaH__


/* El siguiente bloque comprueba que modulo se esta compilando de forma que las variables
globales solo se inicialicen cuando se compile el modulo "modelo.c"
*/

#ifndef __modelo__ // No esta compilando modelo, las variables globales son extern

#define ambito extern

#else // Esta compilando  modelo, las variables globales no son extern (se les asigna memoria)

#define ambito

#endif

// ====================================== DEFINES ========================================================


/**
Definicion de identificadores de colores. Los valores se corresponden con la
posicion en la matriz color.
**/
#define amarillo 0
#define gris 1
#define rojo 2
#define	marron 3
#define celeste 4
#define naranja 5
#define blanco 6
#define colorfeedback 7
#define violeta 7
#define azul 8
#define verde 9
#define negro 10

#define VELDESPCAMARA 0.1
#define TAM_CUADRICULA 100.0
#define RAD_CUADRICULA 0.01

#define MAXNAMES 10
#define MAXOBJETOS 10

#define NGruas 20
#define NCajones 99 // Poner una menos de las que se quieran (100)

#define PI 3.141592 // Constante matemática PI
#define SEPSEL 0.02 // Separación de la marca de selección
#define COLORSEL blanco

#define MAXMUROS 100
#define ALTURA_MURO 3
#define ALTURA_MURO_OC 0.3
#define NOMBRE_MURO 2000
#define GROSOREXT 0.5
#define GROSORINT 0.1
#define GROSORPUERTAINT 0.035
#define DISTVALIDA 0.3

#define MAXMELEMENTOS 50
#define NOMBRE_ELEMENTO 3000
#define MAXMUEBLES 100
#define NOMBRE_MUEBLE 1000
#define RELIEVECAJON 0.03
#define RELIEVECAJONMITAD 0.015

#define TAM_MAX_RUTA 257

//========================================  ESTRUCTURAS DE DATOS ==========================================

typedef enum {seleccionar,paseando,vistaArriba,poniendocajas,enganchando,PintandoCajas,MoviendoCajas,RotandoCajas,
			PoniendoMuros,PoniendoMuebles,PoniendoElementos,MenuMuebles,MenuMuros} ESTADO; // Estados del programa

typedef enum{SALIR,CONFIRMAR,AYUDA,ALTCUAD,ALTPUNTO,ALTROT,ACDEEJES,ACDEMUROS,NUEVAESCENA,ENGANCHAR,DESENGANCHAR,ROTARCAJA,MOVERCAJA,PINTARCAJA,DESDEARRIBA,OPERARGRUA,PASEAR,
			PONERCAJAS,PONERMUROS,PONERMUEBLES,MENUMUEBLES,MENUMUROS,VOLVER,ELIMINAR,
			SILLON,SOFA,FRIGO,CAJONERACOCINA,ENCIMERA,HORNILLA,MESA1X1,SILLA,MESAXL,ESTANTERIAXL,MESANOCHE,CAMA,ARMARIO,BANERA,WC,LAVABO,GUARDAR,GUARDARCOMO,CARGAR,MUROEXT,MUROINT,
			VENTANAUNO,VENTANADOS,PUERTAUNO,PUERTADOS} opciones_menu;  	// Ident. de opciones del menu

typedef struct{
	float x;
	float y;
	float z;
} PUNTO3D;

typedef struct{
	float x;
	float y;
} PUNTO2D;

typedef struct{
	float angY; // Ángulo de giro del brazo
	float posCarro; // Distancia del carro a la torre
	float lCuerda; // Longitud de la cuerda
	float vangY; // Velocidad a la que cambia el ángulo de giro del brazo
	float vposCarro; // Velocidad a la que cambia la distancia del carro a la torre
	float vlCuerda; // Velocidad a la que cambia la longitud de la cuerda
} GRUA;
ambito GRUA grua;

typedef struct{
	float angCabeza; // Ángulo de giro de la cabeza. 0 mira al frente
	float angBrazoIzq; // Ángulo de giro del brazo izquierdo. 0 forma 35º con la vertical
	float angBrazoDer; // Ángulo de giro del brazo derecho. 0 forma 35º con la vertical
	float angPiernas; // Ángulo de giro de las piernas. 0 en vertical
	float posBoca; // Posición de la sonrisa. 0 boca recta
} OPERARIO;
ambito OPERARIO operario;

typedef struct{ // Parámetros de la caja, siempre apoyada en el plano paralelo al plano XZ de altura Y
	float x; // Coordenada x
	float y; // Coordenada y
	float z; // Coordenada z
	float tam_x; // Ancho
	float tam_y; // Alto
	float tam_z; // Profundo
	int color; // Color de la caja
	float rot; // Rotación respecto al eje Y (0 mantiene alineada con el eje X)
} CAJA;

typedef struct{ // Parámetros del mueble, siempre apoyado en el plano paralelo al plano XZ de altura Y
	int tipo; 
	/* El mueble que es: 
	0 sillón
	1 sofá
	2 frigorífico
	3 Mesa 1x1
	4 Silla
	5 MesaXL
	6 EstanteriaXL
	7 Cama
	8 Bañera
	9 WC
	10 Lavabo
	11 Mesita de noche
	12 Armario
	13 Cajonera cocina
	14 Armario de cocina
	15 Hornilla
	*/
	float x; // Coordenada x
	float y; // Coordenada y
	float z; // Coordenada z
	int color; // Color del mueble
	float rot; // Rotación respecto al eje Y (0 mantiene alineada con el eje Z)
} MUEBLE;

typedef struct{ // Posición del vector de muebles
	int libre; // 1 Posición vacía, 0 ocupada
	MUEBLE mueble; // Mueble almacenado
} ALMACENA_MUEBLE;

typedef struct{ // Parámetros de las puertas y ventanas
	int tipo; 
	/* El elemento que es: 
	0 Ventana de 1 hoja
	1 Ventana de 2 hojas
	2 Puerta de muro exterior
	3 Puerta de muro interior
	*/
	float x; // Coordenada x
	float y; // Coordenada y
	float z; // Coordenada z
	int color; // Color del elemento
	float rot; // Rotación respecto al eje Y (0 mantiene alineada con el eje Z)
	int cara; // 0: No Cambiar rotacion 1: Cambiar rotacion
} ELEMENTO;

typedef struct{ // Posición del vector de elementos murales
	int libre; // 1 Posición vacía, 0 ocupada
	ELEMENTO elemento; // elemento almacenado
} ALMACENA_ELEMENTO;

typedef struct{
	int tipo; // 0 Muro exterior, 1 Muro interior (Añadido por problemas de comprobacion del grosor)
	float grosor; // Grosor del muro
	int color; // Color del muro
	PUNTO2D puntos[2];
} MURO;

typedef struct{ // Posición del vector de muros
	int libre; // 1 Posición vacía, 0 ocupada
	MURO muro; // Muro almacenado
} ALMACENA_MURO;

typedef struct {
	int nNames; // Número de identificadores del objeto
	int names[MAXNAMES]; // Vector con las identificadores
	float zmin;
	float zmax;
} OBJETO; // Información de un objeto

typedef struct{
	int nObjetos; // Número de objetos seleccionados
	OBJETO objeto[MAXOBJETOS]; // Vector con los objetos seleccionados
} SELECCION;
ambito SELECCION seleccion; // Información de la operación de selección

typedef struct{
	float x;
	float y;
	float z;
} DIM_MAX_MUEBLE;

//================================================ VARIABLES  =============================================

ambito float color[11][4];	// Paleta de colores (se inicializa en initModel)	

ambito int estado;		// Estado en el que se encuentra el programa

ambito int estado_ejes; // 1 Se muestran los ejes, 0 no se muestran

ambito int estado_cuadricula; // 1 Se muestra la cuadricula, 0 no se muestra

ambito int menu;     	// Identificador del menu glut

ambito CAJA conjunto_cajas[NCajones+1]; // Conjunto de cajas colocadas interactivamente, máximo NCajones+1

ambito int num_cajas_conjunto; // Número de cajas actualmente colocadas

ambito int objetoSeleccionado; // Objeto seleccionado actualmente

ambito int colorActivo; // Color activo

ambito int colgado; // Caja colgada en la grua

ambito PUNTO2D punto_referencia; // Punto de referencia para la inserción de transformaciones

ambito int estado_menu; // 0 Menu contextual normal, 1 menu de muebles, 2 menu de muros

// Variables de la gestión de muros

ambito ALMACENA_MURO conjunto_muros[MAXMUROS]; // Conjunto de muros colocados interactivamente, máximo MAXMUROS

ambito int num_muros_conjunto; // Número de muros actualmente colocados

ambito MURO nuevo_muro; // Muro que se está colocando y no ha sido aún añadido al conjunto

ambito int muro_oculto; // 1: Se ve solo la parte inferior del muro, 0: Se ve normal

/*
0: No añadiendo inicio
1: Añadiendo inicio
2: No añadiendo fin
3: Añadiendo fin
*/
ambito int estado_anadir_muro;

ambito int punto_colocando; // 0: Inicio, 1:Fin

ambito int punto_mover; // 0: Inicio, 1:Fin

// Variables de la gestión de muebles

ambito DIM_MAX_MUEBLE matriz_max_mueble[16]; // Tamaño máximo de cada mueble, ordenado por tipo

ambito ALMACENA_MUEBLE conjunto_muebles[MAXMUEBLES]; // Conjunto de muebles colocados interactivamente, máximo MAXMUEBLES

ambito int num_muebles_conjunto; // Número de muebles actualmente colocados

ambito MUEBLE nuevo_mueble; // Mueble que se está colocando y no ha sido aún añadido al conjunto

ambito int estado_rotacion; // 1: Rotación, 0: Desplazamiento

ambito int estado_anadir_mueble; // 1: Añadiendo, 0: No añadiendo

// Variables de la gestión de elementos murales, comparte variables con los muebles

ambito ALMACENA_ELEMENTO conjunto_elementos[MAXMELEMENTOS]; // Conjunto de elementos colocados interactivamente, máximo MAXMELEMENTOS

ambito int num_elementos_conjunto; // Número de elementos actualmente colocados

ambito ELEMENTO nuevo_elemento; // Elemento que se está colocando y no ha sido aún añadido al conjunto

ambito DIM_MAX_MUEBLE matriz_max_elemento[4]; // Tamaño máximo de cada elemento, ordenado por tipo

ambito int pintar_muro; // 0: No se dibuja el elemento de muro 1: si se dibuja

// Variables de la gestión de archivos

ambito int opcion_guardar; // 0: Guardar, 1: Guardar como...

ambito char save_file[TAM_MAX_RUTA];

/**

Angulos de rotacion de la camara.

**/

ambito float view_rotx, view_roty, view_rotz;

/**

Indica el tipo de proyeccion que se esta realizando. Si esta a uno 
se hace proyeccion perspectiva, en caso contrario se hace paralela.

**/
ambito int VISTA_PERSPECTIVA;

/**

Tamaño para la ventana del mundo en proyeccion paralela.

**/
ambito float ventanaMundoParalela;

/**

Coordenadas x,y de la esquina inferior de la ventana del mundo en vista
paralela.
 
**/
ambito float origenXVentanaMundoParalelo;
ambito float origenYVentanaMundoParalelo;

/**

Distancia de la camara al centro de coordenadas del modelo.

**/
ambito float d;

ambito float x_camara,y_camara,z_camara;

ambito float anchoVentana,altoVentana;

// ============================================== FUNCIONES ================================================

// modelo.c

/**
	Marca la caja seleccionada actualmente
**/
void marcarSeleccion();

/**
	Función auxiliar
**/
void marcarMuro(MURO * m);

/**
	Función auxiliar
**/
void marcarSeleccion2(float pos_x,float pos_y,float pos_z,float rot,float tam_x,float tam_y,float tam_z);

/**
	Función auxiliar
**/
void esquinaSeleccion(int pos,float tam_x,float tam_y,float tam_z);

/**
	Función que engancha la caja seleccionada al gancho
**/
void enganchar();

/**
	Función que desengancha la caja actualmente enganchada al gancho
**/
void desenganchar();

/**
	Función que actualiza los parámetros del modelo
**/
void actualizarModelo();

/**
	Funcion de redibujado. Se ejecuta con los eventos postRedisplay
**/
void Dibuja( void );

/**
	Funcion de fondo
**/
void idle();

/**
	Funcion de inicializacion del modelo y de las variables globales
**/
void initModel();

void inicializaPrograma();

void cuadricula();

/**
	Función que devuelve el valor en radianes del angulo (en grados) introducido
**/
float grados2radianes(float angulo);

/**
	Función que devuelve el valor en grados del angulo (en radianes) introducido
**/
float radianes2grados(float angulo);

// mouse.c

/**
	Funcion de captura de eventos de pulsacion de botones del raton
	boton: identificador del boton pulsado
	estado: estado del boton
	x: posicion del cursor en coordenadas de pantalla
	y: posicion del cursor en coordenadas de pantalla
**/
void clickRaton( int boton, int estado, int x, int y );
	

/**
	Funcion de captura de eventos de desplazamiento de raton
	x: posicion del cursor en coordenadas de pantalla
	y: posicion del cursor en coordenadas de pantalla
**/
void RatonMovido( int x, int y );

/**
	Agrupa las acciones comunes en clickRaton y RatonMovido del estado PoniendoMuebles 
**/
void gestor_PoniendoMuebles(int x,int y);

/**
	Agrupa las acciones comunes en clickRaton y RatonMovido del estado PoniendoElementos 
**/
void gestor_PoniendoElementos(int x,int y);

/**
	Gestiona las acciones de RatonMovido en el estado seleccionar
**/
void gestor_Seleccionar(int x,int y);

/**
	Función de selección
	x: posicion del cursor en coordenadas de pantalla
	y: posicion del cursor en coordenadas de pantalla
**/
void pick(int x, int y);

/**
	Función que ajusta la altura de la caja i para ponerla encima de otras
	i: la posición en el vector de cajas de la caja a manipular
**/
void apilar(int i);

/**
	Obtiene la posición (x,z) del mundo donde se ha pinchado en función de la vista en la que estaba
	x_pantalla: posicion del cursor en coordenadas de pantalla
	y_pantalla: posicion del cursor en coordenadas de pantalla
	x: posicion del cursor en coordenadas del mundo
	z: posicion del cursor en coordenadas del mundo
**/
void dondePincha(int x_pantalla,int y_pantalla,float * x,float * z);

/**
	Función que obtiene las coordenadas del mundo con y = 0 donde se está señalando con el cursor
	x_pantalla: posicion del cursor en coordenadas de pantalla
	y_pantalla: posicion del cursor en coordenadas de pantalla
	x_mundo: posicion del cursor en coordenadas del mundo
	z_mundo: posicion del cursor en coordenadas del mundo
**/
void pincha3D(int x_pantalla, int y_pantalla, float * x_mundo, float * z_mundo);

/**
	Aplica una traslación (dx,dy,dz) al punto pasado como argumento
	p: puntero al punto
	dx: traslación sobre el eje x
	dy: traslación sobre el eje y
	dz: traslación sobre el eje z
**/
void trasladar_punto(PUNTO3D * p,float dx,float dy,float dz);

/**
	Aplica una rotación de ang grados sobre el eje X al punto pasado como argumento
	p: puntero al punto
	ang: ángulo de la rotación en grados
**/
void rotar_punto_ejeX(PUNTO3D * p,float ang);

/**
	Aplica una rotación de ang grados sobre el eje Y al punto pasado como argumento
	p: puntero al punto
	ang: ángulo de la rotación en grados
**/
void rotar_punto_ejeY(PUNTO3D * p,float ang);

/**
	Normaliza un ángulo en grados en el intervalo ]-360,360[
	angulo: El ángulo a normalizar
	Devuelve el ángulo normalizado
**/
float normaliza_angulo(float angulo);

/**
	Calcula la distancia euclídea entre 2 puntos del plano
	x1: Coordenada x del punto 1
	y1: Coordenada y del punto 1
	x2: Coordenada x del punto 2
	y2: Coordenada y del punto 2
**/
float distanciaEuclidea2D(float x1,float y1,float x2,float y2);

/**
	Calcula el ángulo (en grados) necesario para orientar un objeto, que está situado en la posicion pos_x,pos_z con vector de orientación (1,0,0),
	en la dirección del punto x_or,z_or
	pos_x: Coordenada X de la posición del objeto
	pos_z: Coordenada Z de la posición del objeto
	x_or: Coordenada X del punto de orientación
	z_or: Coordenada Z del punto de orientación
	Devuelve el ángulo calculado en grados, o un valor mayor de 360 si coninciden la posición y el punto
**/
float angulo_orientacion(float pos_x,float pos_z,float x_or,float z_or);

// entradaTeclado.c

/**	
	Funcion de captura de eventos pulsacion de tecla correspondiente a caracter alfanumerico
	K: codigo ascii del caracter
	x: posicion del cursor en coordenadas de pantalla cuando se pulso la tecla
	y: posicion del cursor en coordenadas de pantalla cuando se pulso la tecla
**/
void letra (unsigned char k, int x, int y);

/**
	Funcion de captura de eventos pulsacion de caracteres especiales y de control
	K: codigo del control pulsado (se definen como constantes en glut.h)
	x: posicion del cursor en coordenadas de pantalla cuando se pulso la tecla
	y: posicion del cursor en coordenadas de pantalla cuando se pulso la tecla
**/
void especial(int k, int x, int y);


// entradaMenu.c

void seleccionMenu( int opcion );
// Este procedimiento es llamado por el sistema cuando se selecciona una
// opcion del menu glut. El parametro contiene el identificador de la opcion

 
void CreaMenu();
// Este proc. se usa para definir el menu glut. Se llama desde main.


// estructura.c

void caja( float a, float b, float m);
/* Construye un paralelepipedo alineado con los ejes de
dimension a x b x m. El objeto se construye en el semiespacio y>=0, con el origen en el
centro de la base. 
*/


void falsoCilindro(float h, float r);
/*

Crea un paralelepipedo con sombreado como phong (simulando un cilindro)
con centro de las bases en 0,0,0 y 0,h,0, y radio de la base r.

*/

void falsoCono( float h, float r);
/*

Crea una piramide sombreada como un cono con centro de las bases en
0,0,0, cuspide en  0,h,0, y radio de la base r.

*/


void ejes(float a);
/*
Dibuja unos ejes  de tamaño a
*/

// ========================== paralelepipedo ===========================================
/*
void paralelepipedo(float x0, float y0, float z0, 
		    float x1, float y1, float z1, 
		    float a, float b);*/
/*
	Crea un paralelepipedo con centro de las bases en
	x0,y0,z0 y x1,y1,z1, y tamaño de la base axb.
*/


// =========================== creaEstructura ==========================================


void creaEstructura(float h, float a, float b, int n);
/*
Crea un fragmento de estructura de una grua, con centro de las bases en
0,0,0 y 0,h,0. Las secciones de las bases tienen tamanyo axb
n es el numero de niveles que tiene la estructura.

Los perfiles tienen seccion cuadrada, dada por el parametro seccion.
	
Para crear la estructura se utiliza el procedimiento paralelepipedo.
*/

void creaBrazo(float h, float a, int n);
/*
Crea un fragmento del brazo de una grua, con centro una de las aristas 
de los extremos en
0,0,0 y 0,h,0. Los lados de las bases tienen tamanyo a,
n es el numero de tramos.

Los perfiles tienen seccion triangular
	
Para crear la estructura se utiliza el procedimiento paralelepipedo.
*/



void creaTorre(float h, float a, float b, int n);
/*

Crea un fragmento de la terminacion de la estructura de una grua, 
con centro de las bases en
0,0,0 y 0,h,0. Las secciones de las bases tienen tamanyo axb
n es el numero de niveles que tiene la estructura.

Los perfiles tienen seccion cuadrada, dada por el parametro seccion.
	
Para crear la estructura se utiliza el procedimiento paralelepipedo.

*/

void creaGancho( float alto);
/*
Crea un gancho colgando del punto (0,0,0) de tamanyo alto
El gancho siempre esta vertical
*/


// visual.c

void fijaProyeccion();

void transformacionVisualizacion();

void inicializaVentana(GLsizei ancho,GLsizei alto);

void  creaIU();

void drawSeleccionColor();

void DrawPalancas();

void seleccionaPalancas( int boton, int estado, int x, int y);

void muevePalanca( int x, int y);

void seleccionColor( int boton, int estado, int x, int y);


// construcciongrua.c

/**
	Función que dibuja la grúa
**/
void creaGrua();

/**
	Función que dibuja los tirantes de sujeción del contrapeso
**/
void creaTirantesContrapeso();

/**
	Función que dibuja la cabina
**/
void creaCabina();

/**
	Función que dibuja un operario
**/
void creaOperario();

// piezascasa.c

void confirmaMuro();

void inicializacion_muro();

/**
	Inicializa el conjunto de muros
**/
void inicializa_conj_muros();

/**
	Añade un muro al conjunto de muros
	nuevo: El muro a añadir
	Devuelve 0 en caso de éxito, y -1 en caso de fallo
**/
int anadir_muro(MURO * nuevo);

/**
	Elimina un muro del conjunto de muros
	i: El índice del muro dentro del conjunto de muros
**/
void eliminar_muro(int i);

/**
	Pinta el muro pasado como argumento
	m: El muro a pintar
**/
void pintaMuro(MURO * m);

/**
	Devuelve la altura a la que se pintarán los muros
**/
float alturaMuro();

/**
	Pinta una sección de muro entre los puntos p1 y p2
	p1: Extremo de la sección
	p2: El otro extremo de la sección
	ancho: ancho de la sección
	alto: alto de la sección
**/
void pintaSeccion(PUNTO2D p1,PUNTO2D p2,float ancho,float alto);

/**
	Inicializa el conjunto de elementos murales
**/
void inicializa_conj_elementos();

/**
	Pinta el elemento pasado como argumento
	e: El elemento a pintar
**/
void pintaElemento(ELEMENTO * e);

/**
	Añade un elemento al conjunto de elementos
	nuevo: El elemento a añadir
	Devuelve 0 en caso de éxito, y -1 en caso de fallo
**/
int anadir_elemento(ELEMENTO * nuevo);

void confirmaElemento();

/**
	Elimina un elemento del conjunto de elementos
	i: El índice del elemento dentro del conjunto de elementos
**/
void eliminar_elemento(int i);

void inicializacion_elemento();

void confirmaMueble();

void inicializacion_mueble();

/**
	Pinta el mueble pasado como argumento
	m: El mueble a pintar
**/
void pintaMueble(MUEBLE * m);

/**
	Inicializa el conjunto de muebles
**/
void inicializa_conj_muebles();

/**
	Añade un mueble al conjunto de muebles
	nuevo: El mueble a añadir
	Devuelve 0 en caso de éxito, y -1 en caso de fallo
**/
int anadir_mueble(MUEBLE * nuevo);

/**
	Elimina un mueble del conjunto de muebles
	i: El índice del mueble dentro del conjunto de muebles
**/
void eliminar_mueble(int i);

void creaSillon();

void creaSofa();

void creaFrigorifico();

void creaMesa1x1();

void creaSilla();

void creaMesaXL();

void creaEstanteriaXL();

void creaCama();

void creaBanera();

void creaWC();

void creaLavabo();

void creaArmario(int idcolor);

void creaMesitaNoche(int idcolor);

void creaCajon(int num_tir,float alto,float ancho);

void creaCajonera(int idcolor);

void creaEncimera(int idcolor);

void creaHornilla(int idcolor);

void creaVentana1();

void creaVentana2();

void creaPuerta1();

void creaPuerta2();

/**
	Comprueba si la posición x_pos,z_pos está a menos de DISTVALIDA de algún muro,
	en caso negativo devolverá -1,
	en caso afirmativo devolverá el identificador del vector de muros del muro asociado 
	y actualizará las variables x,z que contendrán la posición dentro del muro
**/
int cercaMuro(float x_pos,float z_pos,float * x_cor,float * z_cor,float sep_borde);

/**
	Función de análisis para la anterior, aqui se le pasa el muro a analizar
	Devuelve -1, si no está a menos de DISTVALIDA y la distancia en caso afirmativo
	Hace la actualización de variables descrita en la función anterior
**/
float analizar(float x_pos,float z_pos,float * x_cor,float * z_cor,int idmuro,float sep_borde);

void creaManivela();

// manejo_archivos.c

/**
	Gestiona el proceso de guardado
**/
void guardado();

/**
	Guarda la escena en un archivo
	fich: El nombre del fichero
	Devuelve 0 en caso de éxito y -1 en caso de fallo
**/
int guardar(char * fich);

/**
	Gestiona el proceso de cargado
**/
void cargado();

/**
	Carga la escena desde un archivo con extensión .hb
	fich: El nombre del fichero
	Devuelve 0 en caso de éxito y -1 en caso de fallo
**/
int cargar(char * fich);

/**
	Prepara una nueva escena
**/
void nueva_escena();

/**
	Gestiona la salida de la aplicación
**/
void salida();

#endif
#undef ambito

