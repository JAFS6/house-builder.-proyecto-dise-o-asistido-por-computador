/*	Grua 

	C.A.D. 						Curso 2008-2009
 	
	Codigo base para la realización de las practicas de CAD

	modulo entradaMenu.c
	Gestion de eventos de menu
=======================================================
	J.C. Torres 
	Dpto. Lenguajes y Sistemas Informticos
	(Univ. de Granada, SPAIN)

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details 
 http://www.gnu.org/copyleft/gpl.html

=======================================================
Queda prohibido cobrar canon por la copia de este software

*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>                   // Libreria de utilidades de OpenGL
#include "grua.h"



// Nota: Los acentos han sido suprimidos en la documentacion por compatibilidad
// con las version usada de doxygen. Las letras ñ se han sustituido por "ny"


/** 			void seleccionMenu( int opcion )

Este procedimiento es llamado por glut cuando ocurre un evento de seleccion de una 
opcion del menu. El parametro contiene el identificador de la opcion
 
 Cuando se añadan opciones se debe anyadir un case en el switch para esa opcion.
 
 **/
void seleccionMenu(int opcion)
{
	gluiSetCanvasWindow();
	//gluiOutput("");			// Borra el mensaje actual
	
	switch(opcion)
	{   
		case AYUDA:
			printHelp();
			break;
		case PASEAR:
			VISTA_PERSPECTIVA = 1; 			// Fija perspectiva y recalcula proyeccion
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			fijaProyeccion();
			break;
		case DESDEARRIBA: 
			VISTA_PERSPECTIVA= 0; 			// Fija proyeccion paralela y recalcula proyeccion
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			fijaProyeccion();
			break;
		/*case PONERCAJAS:
			estado = poniendocajas;
			gluiOutput("Colocando cajas");
			break;
		case ENGANCHAR:
			if(colgado == -1){
				estado = enganchando;
				gluiOutput("Elige la caja a enganchar");
				enganchar();
			}
			else{
				gluiOutput("Primero suelte la caja colgada");
			}
			break;
		case DESENGANCHAR:
			if(colgado != -1)
				desenganchar();
			else
				gluiOutput("No hay caja para soltar");
			break;
		case PINTARCAJA:
			estado = PintandoCajas;
			objetoSeleccionado = -1;
			gluiOutput("Elige la caja a colorear");
			break;
		case ROTARCAJA:
			estado = RotandoCajas;
			objetoSeleccionado = -1;
			gluiOutput("Elige la caja a rotar");
			break;
		case MOVERCAJA:
			estado = MoviendoCajas;
			objetoSeleccionado = -1;
			gluiOutput("Elige la caja a mover");
			break;*/
		case MENUMUROS:							// MUROS
			estado = MenuMuros;
			CreaMenu();
			gluiOutput("Seleccione elemento");
			break;
		case MUROEXT:
			nuevo_muro.grosor = GROSOREXT;
			nuevo_muro.tipo = 0;
			inicializacion_muro();
			break;
		case MUROINT:
			nuevo_muro.grosor = GROSORINT;
			nuevo_muro.tipo = 1;
			inicializacion_muro();
			break;
		case MENUMUEBLES:							// MUEBLES
			estado = MenuMuebles;
			CreaMenu();
			gluiOutput("Seleccione mueble");
			break;
		case SILLON:
			inicializacion_mueble();
			nuevo_mueble.tipo = 0;
			break;
		case SOFA:
			inicializacion_mueble();
			nuevo_mueble.tipo = 1;
			break;
		case FRIGO:
			inicializacion_mueble();
			nuevo_mueble.tipo = 2;
			break;
		case MESA1X1:
			inicializacion_mueble();
			nuevo_mueble.tipo = 3;
			break;
		case SILLA:
			inicializacion_mueble();
			nuevo_mueble.tipo = 4;
			break;
		case MESAXL:
			inicializacion_mueble();
			nuevo_mueble.tipo = 5;
			break;
		case ESTANTERIAXL:
			inicializacion_mueble();
			nuevo_mueble.tipo = 6;
			break;
		case CAMA:
			inicializacion_mueble();
			nuevo_mueble.tipo = 7;
			break;
		case BANERA:
			inicializacion_mueble();
			nuevo_mueble.tipo = 8;
			break;
		case WC:
			inicializacion_mueble();
			nuevo_mueble.tipo = 9;
			break;
		case LAVABO:
			inicializacion_mueble();
			nuevo_mueble.tipo = 10;
			break;
		case MESANOCHE:
			inicializacion_mueble();
			nuevo_mueble.tipo = 11;
			break;
		case ARMARIO:
			inicializacion_mueble();
			nuevo_mueble.tipo = 12;
			break;
		case CAJONERACOCINA:
			inicializacion_mueble();
			nuevo_mueble.tipo = 13;
			break;
		case ENCIMERA:
			inicializacion_mueble();
			nuevo_mueble.tipo = 14;
			break;
		case HORNILLA:
			inicializacion_mueble();
			nuevo_mueble.tipo = 15;
			break;
		case PUERTAUNO:							// ELEMENTOS MURALES
			inicializacion_elemento();
			nuevo_elemento.tipo = 2;
			break;
		case PUERTADOS:
			inicializacion_elemento();
			nuevo_elemento.tipo = 3;
			break;
		case VENTANAUNO:							// ELEMENTOS MURALES
			inicializacion_elemento();
			nuevo_elemento.tipo = 0;
			break;
		case VENTANADOS:
			inicializacion_elemento();
			nuevo_elemento.tipo = 1;
			break;
		case VOLVER:
			if(estado == PoniendoMuebles || 
			   estado == MenuMuebles || 
			   estado == PoniendoMuros || 
			   estado == MenuMuros ||
			   estado == PoniendoElementos){
			   
				estado = seleccionar;
				CreaMenu();
				gluiOutput("Editando escena");
			}
			break;
		case ELIMINAR:
			if(objetoSeleccionado >= NOMBRE_ELEMENTO){
				eliminar_elemento(objetoSeleccionado - NOMBRE_ELEMENTO);
				objetoSeleccionado = -1;
			}
			else if(objetoSeleccionado >= NOMBRE_MURO){
				eliminar_muro(objetoSeleccionado - NOMBRE_MURO);
				objetoSeleccionado = -1;
			}
			else if(objetoSeleccionado >= NOMBRE_MUEBLE){
				eliminar_mueble(objetoSeleccionado - NOMBRE_MUEBLE);
				objetoSeleccionado = -1;
			}
			break;
		case CARGAR:
			cargado();
			break;
		case GUARDAR:
			opcion_guardar = 0;
			guardado();
			break;
		case GUARDARCOMO:
			opcion_guardar = 1;
			guardado();
			break;
		case NUEVAESCENA:
			nueva_escena();
			break;
		case ALTROT:
			if(estado == PoniendoElementos){
				if(nuevo_elemento.cara == 0){
					nuevo_elemento.cara = 1;
					nuevo_elemento.rot = normaliza_angulo(nuevo_elemento.rot+180);
				}
				else{
					nuevo_elemento.cara = 0;
					nuevo_elemento.rot = normaliza_angulo(nuevo_elemento.rot+180);
				}
				gluiOutput("Rotado");
			}
			else if(estado == seleccionar && objetoSeleccionado >= NOMBRE_ELEMENTO){
				int indice = objetoSeleccionado - NOMBRE_ELEMENTO;
				if(conjunto_elementos[indice].elemento.cara == 0){
					conjunto_elementos[indice].elemento.cara = 1;
					conjunto_elementos[indice].elemento.rot = normaliza_angulo(conjunto_elementos[indice].elemento.rot+180);
				}
				else{
					conjunto_elementos[indice].elemento.cara = 0;
					conjunto_elementos[indice].elemento.rot = normaliza_angulo(conjunto_elementos[indice].elemento.rot+180);
				}
				gluiOutput("Rotado");
			}
			else{
				if(estado_rotacion == 0){
					estado_rotacion = 1;
					gluiOutput("Rotando");
				}
				else{
					estado_rotacion = 0;
					gluiOutput("Moviendo");
				}
			}
			break;
		case ALTPUNTO:
			if(estado == seleccionar){
				if(punto_mover == 0){
					punto_mover = 1;
				}
				else{
					punto_mover = 0;
				}
			}
			break;
		case ACDEEJES:
			if(estado_ejes == 0){
				estado_ejes = 1;
			}
			else{
				estado_ejes = 0;
			}
			break;
		case ALTCUAD:
			if(estado_cuadricula == 0){
				estado_cuadricula = 1;
			}
			else{
				estado_cuadricula = 0;
			}
			break;
		case ACDEMUROS:
			if(muro_oculto == 0){
				muro_oculto = 1;
			}
			else{
				muro_oculto = 0;
			}
			break;
		case CONFIRMAR:
			if(estado == PoniendoMuebles){
				confirmaMueble();
			}
			else if(estado == PoniendoMuros){
				confirmaMuro();
			}
			else if(estado == PoniendoElementos){
				confirmaElemento();
			}
			break;
		case SALIR:
			salida();
			break;
	}
	gluiPostRedisplay(); 	// Si se ha modificado algo que cambia el estado del
							// modelo se debe actualizar la pantalla
}


/** 

Crea el menu.pop-up de la ventana grafica

Este procedimiento debe llamarse para crear el menu pop-up de glut (normalmente al inicializar el
programa).
 
Para anyadir opciones al menu anyadir una llamada glutAddMenuEntry e incluir valores del tipo enumerado en la declaracion de opciones_menu.

 **/
void CreaMenu()
{
	menu = glutCreateMenu(seleccionMenu);
	
	if(estado == PoniendoMuros || estado == PoniendoElementos || estado == MenuMuros){
		estado_menu = 2;
		glutAddMenuEntry("Muro exterior",MUROEXT);
		glutAddMenuEntry("Muro interior",MUROINT);
		glutAddMenuEntry("Puerta de muro exterior",PUERTAUNO);
		glutAddMenuEntry("Puerta de muro interior",PUERTADOS);
		glutAddMenuEntry("Ventana simple",VENTANAUNO);
		glutAddMenuEntry("Ventana doble",VENTANADOS);
		glutAddMenuEntry("Confirmar  (Enter)",CONFIRMAR);
		glutAddMenuEntry("Dejar herramienta (ESC)",VOLVER);
	}
	else if(estado == PoniendoMuebles || estado == MenuMuebles){
		estado_menu = 1;
		glutAddMenuEntry("Silla",SILLA);
		glutAddMenuEntry("Sillon",SILLON);
		glutAddMenuEntry("Sofa",SOFA);
		glutAddMenuEntry("Mesa pequena",MESA1X1);
		glutAddMenuEntry("Mesa grande",MESAXL);
		glutAddMenuEntry("Estanteria",ESTANTERIAXL);
		glutAddMenuEntry("Cama",CAMA);
		glutAddMenuEntry("Mesita de noche",MESANOCHE);
		glutAddMenuEntry("Armario",ARMARIO);
		glutAddMenuEntry("Banera",BANERA);
		glutAddMenuEntry("WC",WC);
		glutAddMenuEntry("Lavabo",LAVABO);
		glutAddMenuEntry("Frigorifico",FRIGO);
		glutAddMenuEntry("Cajonera de cocina",CAJONERACOCINA);
		glutAddMenuEntry("Armario de cocina",ENCIMERA);
		glutAddMenuEntry("Hornilla",HORNILLA);
		glutAddMenuEntry("Confirmar  (Enter)",CONFIRMAR);
		glutAddMenuEntry("Dejar herramienta (ESC)",VOLVER);
	}
	else{
		estado_menu = 0;
		/*glutAddMenuEntry("Colocar Caja",PONERCAJAS);
		glutAddMenuEntry("Colorear",PINTARCAJA);
		sglutAddMenuEntry("Enganchar",ENGANCHAR);
		glutAddMenuEntry("Soltar",DESENGANCHAR);
		glutAddMenuEntry("Rotar caja",ROTARCAJA);
		glutAddMenuEntry("Mover caja",MOVERCAJA);*/
		glutAddMenuEntry("Vista 2D  (2)",DESDEARRIBA);	// Cada llamada lleva el nombre de la oopcion 
		glutAddMenuEntry("Vista 3D  (3)",PASEAR);		// y el identificador que se devuelve al usarla
		glutAddMenuEntry("Muros",MENUMUROS);
		glutAddMenuEntry("Muebles",MENUMUEBLES);
		glutAddMenuEntry("Eliminar  (SUP)",ELIMINAR);
	}
	
	glutAttachMenu(GLUT_RIGHT_BUTTON);	// Indica que el boton derecho abre el menu.
}

