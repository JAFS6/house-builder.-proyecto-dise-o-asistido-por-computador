/*	Grua 

	C.A.D. 						Curso 2008-2009
 	
	Codigo base para la realización de las practicas de CAD

	modulo mouse.c
	Gestion de eventos de raton
=======================================================
	J.C. Torres 
	Dpto. Lenguajes y Sistemas Informticos
	(Univ. de Granada, SPAIN)

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details 
 http://www.gnu.org/copyleft/gpl.html

=======================================================
Queda prohibido cobrar canon por la copia de este software

*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>                   // Libreria de utilidades de OpenGL
#include "grua.h"

int estado_boton_izquierdo; // Estado del botón izquierdo del ratón: 1 Pulsado 0 No pulsado

/**	 void clickRaton( int boton, int estado, int x, int y )

Procedimiento para gestionar los eventos de pulsacion de los botones del raton.

Argumentos:

boton: Codigo glut del boton que ha manipulado el usuario.

est: Estado en el que esta el boton que se ha pulsado.

x,y: Posicion, en coordenadas de pantalla, en que se encuantra el cursor.

**/

void clickRaton(int boton, int est, int x, int y)
{	
	if(boton == GLUT_LEFT_BUTTON){ // Botón izquierdo
		if(est == GLUT_DOWN){ // Pulsar el botón izquierdo
			estado_boton_izquierdo = 1;
			if(estado == seleccionar /*||
			   estado == PintandoCajas ||
			   estado == enganchando || 
			   estado == MoviendoCajas || 
			   estado == RotandoCajas*/){
				pick(x,y);
			}
			/*if(estado == poniendocajas){
				if(num_cajas_conjunto < NCajones){		
					++num_cajas_conjunto;
				}
				else{
					gluiOutput("Numero maximo alcanzado");
				}
				conjunto_cajas[num_cajas_conjunto].tam_x = 2;
				conjunto_cajas[num_cajas_conjunto].tam_y = 2;
				conjunto_cajas[num_cajas_conjunto].tam_z = 2;
				conjunto_cajas[num_cajas_conjunto].color = colorActivo;
				conjunto_cajas[num_cajas_conjunto].rot = 0;
				
				dondePincha(x,y,&(conjunto_cajas[num_cajas_conjunto].x),&(conjunto_cajas[num_cajas_conjunto].z));
				
				apilar(num_cajas_conjunto);
				glutSetCursor(GLUT_CURSOR_NONE);
			}
			else if(estado == PintandoCajas){
				if(objetoSeleccionado != -1){
					gluiOutput("Elige el color");
				}
				else{
					gluiOutput("Elige la caja a colorear");
				}
			}
			else if(estado == RotandoCajas){
				if(objetoSeleccionado != -1){
					punto_referencia.x = x;
					punto_referencia.y = y;
					gluiOutput("Rotando");
					glutSetCursor(GLUT_CURSOR_NONE);
				}
			}
			else if(estado == MoviendoCajas){
				if(objetoSeleccionado != -1){
					punto_referencia.x = x;
					punto_referencia.y = y;
					gluiOutput("Moviendo");
					glutSetCursor(GLUT_CURSOR_NONE);
				}
			}
			else*/
			if(estado == PoniendoMuebles){
				if(estado_anadir_mueble == 0){
					estado_anadir_mueble = 1;
					gluiOutput("Situe el mueble");
				}
				gestor_PoniendoMuebles(x,y); //********************** GESTOR MUEBLES **********************
			}
			else if(estado == PoniendoMuros){
				if(estado_anadir_muro == 0){
					estado_anadir_muro = 1;
				}
				else if(estado_anadir_muro == 2){
					estado_anadir_muro = 3;
				}
				dondePincha(x,y,&(nuevo_muro.puntos[punto_colocando].x),&(nuevo_muro.puntos[punto_colocando].y));
			}
			else if(estado == PoniendoElementos){
				if(estado_anadir_mueble == 0){
					estado_anadir_mueble = 1;
					gluiOutput("Situe el elemento");
				}
				gestor_PoniendoElementos(x,y); //********************** GESTOR ELEMENTOS MURALES **********************
			}
		}
		else if(est == GLUT_UP){ // Soltar el botón izquierdo
			estado_boton_izquierdo = 0;
			/*if(estado == poniendocajas){
				glutSetCursor(GLUT_CURSOR_LEFT_ARROW);
			}
			else if(estado == RotandoCajas){
				objetoSeleccionado = -1;
				gluiOutput("Elige la caja a rotar");
				glutSetCursor(GLUT_CURSOR_LEFT_ARROW);
			}
			else if(estado == MoviendoCajas){
				objetoSeleccionado = -1;
				gluiOutput("Elige la caja a mover");
				glutSetCursor(GLUT_CURSOR_LEFT_ARROW);
			}*/
		}
	}
	glutPostRedisplay();
}


/**	void RatonMovido( int x, int y )

Procedimiento para gestionar los eventos de movimiento del raton.

Argumentos:

x,y: Posicion, en coordenadas de pantalla, en que se encuantra el cursor.

**/

void RatonMovido( int x, int y )
{
	if(estado_boton_izquierdo == 1){
		if(estado == seleccionar){
			gestor_Seleccionar(x,y); //********************** GESTOR SELECCIONAR **********************
		}
		else if(estado == PoniendoMuebles){
			gestor_PoniendoMuebles(x,y); //********************** GESTOR MUEBLES **********************
		}
		else if(estado == PoniendoMuros){
			dondePincha(x,y,&(nuevo_muro.puntos[punto_colocando].x),&(nuevo_muro.puntos[punto_colocando].y));
		}
		else if(estado == PoniendoElementos){
			gestor_PoniendoElementos(x,y); //********************** GESTOR ELEMENTOS MURALES **********************
		}
		/*else if(estado == poniendocajas){
			dondePincha(x,y,&(conjunto_cajas[num_cajas_conjunto].x),&(conjunto_cajas[num_cajas_conjunto].z));
			apilar(num_cajas_conjunto);
		}
		else if(estado == RotandoCajas){
			if(objetoSeleccionado != -1){
				conjunto_cajas[objetoSeleccionado].rot += 0.05*((180*atan2(-(y - punto_referencia.y),x - punto_referencia.x))/PI);
				conjunto_cajas[objetoSeleccionado].rot = normaliza_angulo(conjunto_cajas[objetoSeleccionado].rot);
				punto_referencia.x = x;
				punto_referencia.y = y;
				apilar(objetoSeleccionado);
			}
		}
		else if(estado == MoviendoCajas){
			if(objetoSeleccionado != -1){
				dondePincha(x,y,&(conjunto_cajas[objetoSeleccionado].x),&(conjunto_cajas[objetoSeleccionado].z));
				apilar(objetoSeleccionado);
			}
		}*/
	}
	glutPostRedisplay();
}

void gestor_PoniendoMuebles(int x,int y)
{
	float aux_x,aux_z;
	
	if(estado_rotacion == 0){
		dondePincha(x,y,&(nuevo_mueble.x),&(nuevo_mueble.z));
	}
	else{
		dondePincha(x,y,&(aux_x),&(aux_z));
		nuevo_mueble.rot = angulo_orientacion(nuevo_mueble.x,nuevo_mueble.z,aux_x,aux_z);
	}
}

void gestor_PoniendoElementos(int x,int y)
{
	float aux_x,aux_z;
	float sep_borde = matriz_max_elemento[nuevo_elemento.tipo].z / 2;
	dondePincha(x,y,&(nuevo_elemento.x),&(nuevo_elemento.z));
	int muro_objetivo = cercaMuro(nuevo_elemento.x,nuevo_elemento.z,&(aux_x),&(aux_z),sep_borde);
	
	if(muro_objetivo == -1){
		pintar_muro = 0;
		gluiOutput("Situe el elemento");
	}
	else if((conjunto_muros[muro_objetivo].muro.tipo == 1 && nuevo_elemento.tipo == 3) ||
			(conjunto_muros[muro_objetivo].muro.tipo == 0 && (nuevo_elemento.tipo == 0 || nuevo_elemento.tipo == 1 || nuevo_elemento.tipo == 2))){
		pintar_muro = 1;
		nuevo_elemento.x = aux_x;
		nuevo_elemento.z = aux_z;
		nuevo_elemento.rot = normaliza_angulo(angulo_orientacion(conjunto_muros[muro_objetivo].muro.puntos[0].x,
															   conjunto_muros[muro_objetivo].muro.puntos[0].y,
															   nuevo_elemento.x,nuevo_elemento.z) -90);
		if(nuevo_elemento.cara == 1){
			nuevo_elemento.rot = normaliza_angulo(nuevo_elemento.rot+180);
		}
		gluiOutput("Situe el elemento");
	}
	else{
		pintar_muro = 0;
		if(nuevo_elemento.tipo == 3){
			gluiOutput("Colocar sobre muro interior");
		}
		else{
			gluiOutput("Colocar sobre muro exterior");
		}
	}
}

void gestor_Seleccionar(int x,int y)
{
	float aux_x,aux_z,aux_x2,aux_z2;
	int indice;
	
	if(objetoSeleccionado >= NOMBRE_ELEMENTO){
		indice = objetoSeleccionado-NOMBRE_ELEMENTO;
		dondePincha(x,y,&(aux_x),&(aux_z));
		float sep_borde = matriz_max_elemento[conjunto_elementos[indice].elemento.tipo].z / 2;
		int muro_objetivo = cercaMuro(aux_x,aux_z,&aux_x2,&aux_z2,sep_borde);
		
		if(muro_objetivo == -1){
			gluiOutput("Situe el elemento");
		}
		else if((conjunto_muros[muro_objetivo].muro.tipo == 1 && conjunto_elementos[indice].elemento.tipo == 3) || 
				(conjunto_muros[muro_objetivo].muro.tipo == 0 && (conjunto_elementos[indice].elemento.tipo == 0 || conjunto_elementos[indice].elemento.tipo == 1 || conjunto_elementos[indice].elemento.tipo == 2))){
			conjunto_elementos[indice].elemento.x = aux_x2;
			conjunto_elementos[indice].elemento.z = aux_z2;
			conjunto_elementos[indice].elemento.rot = normaliza_angulo(
														angulo_orientacion(conjunto_muros[muro_objetivo].muro.puntos[0].x,
																		   conjunto_muros[muro_objetivo].muro.puntos[0].y,
														conjunto_elementos[indice].elemento.x,conjunto_elementos[indice].elemento.z)-90);
			if(conjunto_elementos[indice].elemento.cara == 1){
				conjunto_elementos[indice].elemento.rot = normaliza_angulo(conjunto_elementos[indice].elemento.rot+180);
			}
			gluiOutput("Editando escena");
		}
		else{
			if(conjunto_elementos[indice].elemento.tipo == 3){
				gluiOutput("Colocar sobre muro interior");
			}
			else{
				gluiOutput("Colocar sobre muro exterior");
			}
		}
	}
	else if(objetoSeleccionado >= NOMBRE_MURO){
		indice = objetoSeleccionado-NOMBRE_MURO;
		dondePincha(x,y,&(conjunto_muros[indice].muro.puntos[punto_mover].x),
						&(conjunto_muros[indice].muro.puntos[punto_mover].y));
	}
	else if(objetoSeleccionado >= NOMBRE_MUEBLE){
		indice = objetoSeleccionado-NOMBRE_MUEBLE;
		
		if(estado_rotacion == 0){
			dondePincha(x,y,&(conjunto_muebles[indice].mueble.x),&(conjunto_muebles[indice].mueble.z));
		}
		else{
			dondePincha(x,y,&(aux_x),&(aux_z));
			conjunto_muebles[indice].mueble.rot = angulo_orientacion(conjunto_muebles[indice].mueble.x,
																	 conjunto_muebles[indice].mueble.z,aux_x,aux_z);
		}
	}
}

void pick(int x, int y)
{
	int n = 256;
	GLuint buff[n];
	int i,b,j;
	glSelectBuffer (n, buff);
	GLint hits, viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);
	
	// Activar modo SELECT
	glRenderMode(GL_SELECT);
	// Inicializamos la pila de nombres
	glInitNames();
	// Fijamos la transformación de proyección
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// indicando como primera transformación (la última que se realizará), la transformación a la ventana de selección, en torno al cursor.
	gluPickMatrix(x,viewport[3] - y,5.0,5.0,viewport);
	// Fijamos el resto de la transformación de visualización
	fijaProyeccion();
	// Realizamos el dibujo
	Dibuja();
	// Restauramos la transformación de visualización
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	fijaProyeccion();
	// Volvemos al modo GL_RENDER y obtenemos el número de componentes del modelo seleccionados
	hits = glRenderMode(GL_RENDER);
	
	if(hits > 0){
		seleccion.nObjetos = hits;
		b=0;
		for(i=0; i<seleccion.nObjetos; ++i){
			seleccion.objeto[i].nNames = buff[b];
			++b;
			seleccion.objeto[i].zmin = buff[b];
			++b;
			seleccion.objeto[i].zmax = buff[b];
			++b;
			for(j=0; j<(seleccion.objeto[i].nNames); ++j){
				seleccion.objeto[i].names[j] = buff[b];
				++b;
			}
		}
		// Buscar la menor z, y esa será la caja seleccionada
		objetoSeleccionado = -1;
		float minz = -1;
		for(i=0; i<seleccion.nObjetos; ++i){
			if((seleccion.objeto[i].nNames > 0) && (minz == -1 || seleccion.objeto[i].zmin < minz)){
				minz = seleccion.objeto[i].zmin;
				objetoSeleccionado = seleccion.objeto[i].names[0];
			}
		}
	}
	//printf("\nobjetoSeleccionado: %i\n",objetoSeleccionado);
}

void apilar(int i)
{
	int j;
	float distancia;
	conjunto_cajas[i].y = 0;
	// Para hacer que se suba en la base de la grúa
	if(abs(conjunto_cajas[i].x) < 5 + (conjunto_cajas[i].tam_x/2) && abs(conjunto_cajas[i].z) < 5 + (conjunto_cajas[i].tam_x/2))
		conjunto_cajas[i].y += 1;
	
	// Apilado
	for(j=0; j<=num_cajas_conjunto; ++j){
		if(j != i && j != colgado){
			distancia = sqrt(pow(conjunto_cajas[i].x - conjunto_cajas[j].x,2) +
							 pow((conjunto_cajas[i].y + (conjunto_cajas[i].tam_y/2))-(conjunto_cajas[j].y + (conjunto_cajas[j].tam_y/2)),2) +
							 pow(conjunto_cajas[i].z - conjunto_cajas[j].z,2)
							 );
			if(distancia <= (conjunto_cajas[i].tam_y/2)+(conjunto_cajas[j].tam_y/2))
				conjunto_cajas[i].y += conjunto_cajas[j].tam_y;
		}
	}
}

void dondePincha(int x_pantalla,int y_pantalla,float * x,float * z)
{
	if(VISTA_PERSPECTIVA == 0){
		*x = ((x_pantalla/anchoVentana-0.5)*ventanaMundoParalela)+origenXVentanaMundoParalelo; 
		*z = ((y_pantalla/altoVentana-0.5)*ventanaMundoParalela)*altoVentana/anchoVentana-origenYVentanaMundoParalelo;
	}
	else{
		pincha3D(x_pantalla,y_pantalla,x,z);
	}
}

void pincha3D(int x_pantalla, int y_pantalla, float * x_mundo, float * z_mundo)
{
	float calto; // altura de la ventana corregida
	if(anchoVentana > 0)
  		calto = altoVentana/anchoVentana;
  	else
  		calto = 1;
		
	PUNTO3D punto_pincha,pos_cam;
	pos_cam.x = 0;
	pos_cam.y = 0;
	pos_cam.z = 0;
	
	// Ajustamos el x,y de pantalla al x,y del plano de proyección delantero
	punto_pincha.x = ((x_pantalla/(anchoVentana-1))*2)-1;
	punto_pincha.y = -(((y_pantalla/( altoVentana-1))*2)-calto);
	punto_pincha.z = -1.5; // Distancia entre la cámara y el plano de proyección delantero
	
	// Aplicamos las transformaciones de la transformacionVisualizacion en orden inverso a los 2 puntos
	trasladar_punto(&pos_cam,0,0,1.5);
	trasladar_punto(&punto_pincha,0,0,1.5);
	
	rotar_punto_ejeX(&pos_cam,-view_rotx);
	rotar_punto_ejeX(&punto_pincha,-view_rotx);
	
	rotar_punto_ejeY(&pos_cam,-view_roty);
	rotar_punto_ejeY(&punto_pincha,-view_roty);
	
	trasladar_punto(&pos_cam,x_camara,y_camara,z_camara);
	trasladar_punto(&punto_pincha,x_camara,y_camara,z_camara);
	
	// Calculamos la recta que pasa por los 2 puntos
	PUNTO3D orientacion;
	
	orientacion.x = pos_cam.x - punto_pincha.x;
	orientacion.y = pos_cam.y - punto_pincha.y;
	orientacion.z = pos_cam.z - punto_pincha.z;
	
	/*
	Calcular la recta que pasa por el punto pos_cam y es paralela al vector orientacion
	
	La ecuación vectorial de la recta es:
	
	(x,y,z) = (pos_cam.x,pos_cam.y,pos_cam.z) + lambda (orientacion.x,orientacion.y,orientacion.z)
	
	Vamos a utilizar las ecuaciones paramétricas:
	
	x = pos_cam.x + lambda *(orientacion.x)
	
	y = pos_cam.y + lambda *(orientacion.y)
	
	z = pos_cam.z + lambda *(orientacion.z)
	
	Ahora hacemos la intersección de la recta con el plano y = 0 (suelo del mundo)
	
	Por tanto, en las ecuaciones paramétricas sustituimos 'y' por 0 y obtenemos lambda:
	*/
	
	float lambda = - (pos_cam.y/orientacion.y);
	
	// Ya solo queda sustituir lambda en las ecuaciones paramétricas de la recta para obtener el punto (x,z) del suelo donde se ha hecho clic
	
	*x_mundo = pos_cam.x + (lambda *orientacion.x);
	
	*z_mundo = pos_cam.z + (lambda *orientacion.z);
}

void trasladar_punto(PUNTO3D * p,float dx,float dy,float dz)
{
	(*p).x += dx;
	(*p).y += dy;
	(*p).z += dz;
}

void rotar_punto_ejeX(PUNTO3D * p,float ang)
{
	float angulo_radianes = grados2radianes(ang);
	//[x,y*cos(a)-z*sin(a),y*sin(a)+z*cos(a)]
	float y_aux = (((*p).y)*cos(angulo_radianes))-(((*p).z)*sin(angulo_radianes));
	float z_aux = (((*p).y)*sin(angulo_radianes))+(((*p).z)*cos(angulo_radianes));
	
	(*p).y = y_aux;
	(*p).z = z_aux;
}

void rotar_punto_ejeY(PUNTO3D * p,float ang)
{
	float angulo_radianes = grados2radianes(ang);
	//[x*cos(a)+z*sin(a),y,-x*sin(a)+z*cos(a)]
	float x_aux = (  ((*p).x) *cos(angulo_radianes))+(((*p).z)*sin(angulo_radianes));
	float z_aux = ((-((*p).x))*sin(angulo_radianes))+(((*p).z)*cos(angulo_radianes));
	
	(*p).x = x_aux;
	(*p).z = z_aux;
}

float normaliza_angulo(float angulo)
{
	float res = angulo;
	while(res >= 360){
		res -= 360;
	}
	while(res <= -360){
		res += 360;
	}
	return res;
}

/**
	Calcula la distancia euclídea entre 2 puntos del plano
	x1: Coordenada x del punto 1
	y1: Coordenada y del punto 1
	x2: Coordenada x del punto 2
	y2: Coordenada y del punto 2
**/
float distanciaEuclidea2D(float x1,float y1,float x2,float y2)
{
	return (sqrt((pow(x2-x1,2))+(pow(y2-y1,2))));
}

float angulo_orientacion(float pos_x,float pos_z,float x_or,float z_or)
{	
	if(pos_x == x_or && pos_z == z_or){ // No se puede calcular ángulo
		return (500.0);
	}
	if(pos_x == x_or){ // Perpendicular, ángulo 90 o -90
		if(z_or < pos_z){
			return (90.0);
		}
		return (-90.0);
	}
	if(pos_z == z_or){ // Paralela, ángulo 0 o 180
		if(x_or < pos_x){
			return (180.0);
		}
		return (0.0);
	}
	
	/*
	Comprobados los casos especiales, vamos a calcular al ángulo
	mediante el uso de la pendiende de la recta que pasa por el punto de posición y el de orientación
	*/
	float angulo = atan((z_or - pos_z)/(x_or - pos_x));
	angulo = -angulo; // Ajustamos sentido de giro
	angulo = radianes2grados(angulo);
	/*
	Esto calcula el ángulo mínimo entre las rectas, así que ahora hay que ajustarlo en función de la posición de las rectas
	*/
	if(x_or > pos_x){
		return angulo;
	}
	else{ // x_or < pos_x
		return (180 + angulo);
	}
	return 0;
}


